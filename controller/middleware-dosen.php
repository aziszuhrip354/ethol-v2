<?php 
     include("../controller/auth.php");
     include("../controller/helper-func.php");
     session_start();

     $user = $_SESSION['users'];
     if($user['type_user'] != 1) {
        $_SESSION['error_message'] = "Maaf kamu hanya bisa mengakses role mahasiswa";
        if($user['type_user'] == 0) {
         $user = 'admin';
        } else {
         $user = 'mahasiswa';
        }
        header("location: ../../view/$user/dashboard.php");
     }
?>