<?php 
    include("../controller/auth.php");
    include("../controller/helper-func.php");
    session_start();
    $idTugas = $_POST['idTugas'];
    $nilai = (int)$_POST['beri_nilai_nilai'];
    $corrected_at = date("Y-m-d H:i:s");
    if($nilai > 0) {
        $sqlUpdate = sqlUpdate($connectingToDb, "submit_tugas","score='$nilai', corrected_at='$corrected_at'"," WHERE id='$idTugas'");
        if($sqlUpdate) {
            $_SESSION['success_message'] = "Berhasil memberi nilai";
            header("location: ".$_SERVER['HTTP_REFERER']);
        } else {
            dd(mysqli_error($connectingToDb));
            $_SESSION['error_message'] = "Gagal memberi nilai";
            header("location: ".$_SERVER['HTTP_REFERER']);
        }
    } else {
        $_SESSION['error_message'] = "Nilai minimal 0, tidak boleh minus";
        header("location: ".$_SERVER['HTTP_REFERER']);
    }
?>