<?php 
    include("../controller/auth.php");
    include("../controller/helper-func.php");
    session_start();

    $idUser = $_POST['idUser'];
    $idMahasiswa = $_POST['idMahasiswa'];
    $namaMahasiswa = $_POST['nama_mahasiswa'];
    $emailMahasiswa = $_POST['email'];
    $jenisKelamin = $_POST['jenis_kelamin'];
    $nrp = $_POST['nrp'];
    $kota = $_POST['kota'];
    $password = $_POST['password'];

    $sqlSelectUser = sqlSelect($connectingToDb, "*","users","WHERE id='".$idUser."'");
    $sqlSelectUser = mysqli_fetch_assoc($sqlSelectUser);
    $sqlSelectMahasiswa = sqlSelect($connectingToDb, "*","mahasiswa","WHERE id='".$idMahasiswa."'");
    $sqlSelectMahasiswa = mysqli_fetch_assoc($sqlSelectMahasiswa);
    
    $checkingUserEmail = sqlSelect($connectingToDb, "*","users","WHERE email='".$emailMahasiswa."' AND id!='$idUser'");
    $checkingUserEmail = mysqli_fetch_assoc($checkingUserEmail);

    if($checkingUserEmail) {
        $_SESSION['error_message'] = 'Maaf... email telah digunakan';
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else {
        if(!$sqlSelectUser) {
            $_SESSION['error_message'] = 'Maaf... data user tidak valid';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        } else {
            if(!$password) {
                $updateUser = sqlUpdate($connectingToDb, "users"," email='$emailMahasiswa'"," WHERE id='$idUser'");
            } else {
                $password = password_hash($password, PASSWORD_DEFAULT);
                $updateUser = sqlUpdate($connectingToDb, "users"," email='$emailMahasiswa',password='$password'"," WHERE id='$idUser'");
            }
            if(!$updateUser) {
                $_SESSION['error_message'] = 'Maaf... data user gagal diupdate';
                header('Location: '.$_SERVER['HTTP_REFERER']);
            }
        }

        if(!$sqlSelectMahasiswa) {
            $_SESSION['error_message'] = 'Maaf... data mahasiswa tidak valid';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        } else {
            $updateMahasiswa = sqlUpdate($connectingToDb, "mahasiswa","nama='$namaMahasiswa', nrp='$nrp', jenis_kelamin='$jenisKelamin', kota_asal='$kota'"," WHERE id='$idMahasiswa'");
            $sqlCheckNrpValid = sqlSelect($connectingToDb, "*","mahasiswa","WHERE nrp='".$nrp."' AND id!='".$idMahasiswa."'");
            $sqlCheckNrpValid = mysqli_fetch_assoc($sqlCheckNrpValid);
            if($sqlCheckNrpValid) {
                $_SESSION['error_message'] = 'Maaf... data nrp telah digunakan mahasiswa lain';
                header('Location: '.$_SERVER['HTTP_REFERER']);
            } else {
                if(!$updateMahasiswa) {
                    $_SESSION['error_message'] = 'Maaf... data mahasiswa gagal diupdate';
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                }
            }
                
        }

        $_SESSION['success_message'] = 'Data mahasiswa '.$namaMahasiswa.' berhasil diupdate';
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
    

?>