<?php
    include('auth.php');
    include('helper-func.php');
    session_start();
    $getDataIdMember = $_POST['idMember'];
    $namaKelas = $_POST['nama_kelas'];
    $angkatan = $_POST['angkatan'];
    $idWalas = $_POST['wali_kelas'];

    if($getDataIdMember) {
        $sqlInsert = "INSERT INTO kelas (nama_kelas, angkatan)
                 VALUES ('$namaKelas','$angkatan')";
        $createClass = mysqli_query($connectingToDb, $sqlInsert);
        $idKelas = mysqli_insert_id($connectingToDb);

        if($idKelas) {
            foreach($getDataIdMember as $data) {
                $sqlInsert = "INSERT INTO anggota_kelas (mahasiswa_id, wali_kelas_id, kelas_id)
                            VALUES ('$data','$idWalas','$idKelas')";
                $createAnggotKelas = mysqli_query($connectingToDb, $sqlInsert);
                if(!$createAnggotKelas) {
                    $_SESSION['error_message'] = 'Maaf gagal membuat anggota kelas';
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                }
            }
        } else {
            $_SESSION['error_message'] = 'Maaf gagal membuat kelas';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }

        $_SESSION['success_message'] = 'Berhasil membuat kelas '.$namaKelas;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else {
        $_SESSION['error_message'] = 'Maaf silahkan cari dan tentukan nama mahasiswa yang dimasukan ke kelas '.$namaKelas;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }

    
?>