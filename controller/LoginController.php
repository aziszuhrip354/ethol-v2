<?php
    include($_SERVER["DOCUMENT_ROOT"]."/project-akhir/controller/helper-func.php");
    include(path("auth.php"));
    session_start();
    $email = $_POST['email'];
    $password = $_POST['password'];

    
    $sqlFind = "SELECT * FROM users WHERE email='$email' LIMIT 1";
    $queryFind = mysqli_query($connectingToDb, $sqlFind);
    $checkData = mysqli_num_rows($queryFind);
    $checkingUserFormDb = mysqli_fetch_assoc($queryFind);

    if($checkData == 0) {
        $_SESSION['error_message'] = 'Maaf... email Anda tidak terdaftar';
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else {
        if(!password_verify($password, $checkingUserFormDb["password"])) {
            $_SESSION['error_message'] = 'Maaf... password Anda salah';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        } else {
            $getDataUsers = $checkingUserFormDb;
            $_SESSION['users'] = $getDataUsers;
            if($getDataUsers["type_user"] == 0) {
                header("Location: "."../view/admin/dashboard.php");
            } else if($getDataUsers["type_user"] == 1) {
                $sqlSelectDosen = sqlSelect($connectingToDb, "*","dosen","WHERE user_id='".$getDataUsers['id']."'");
                $sqlSelectDosen = mysqli_fetch_assoc($sqlSelectDosen);
                $sqlSelectKelasWali = sqlSelect($connectingToDb, "k.nama_kelas","users u","
                                                INNER JOIN dosen d ON d.user_id='".$getDataUsers['id']."' 
                                                INNER JOIN anggota_kelas ak ON ak.wali_kelas_id=d.id 
                                                INNER JOIN kelas k ON k.id=ak.kelas_id");
                $sqlSelectKelasWali = mysqli_fetch_assoc($sqlSelectKelasWali);    
                $_SESSION['users']['dosen'] = $sqlSelectDosen;
                $_SESSION['nama_kelas'] = $sqlSelectKelasWali['nama_kelas'];
                header("Location: "."../view/dosen/dashboard.php");
            } else {
                $sqlSelectMhs = sqlSelect($connectingToDb, "*","mahasiswa","WHERE user_id='".$getDataUsers['id']."'");
                $sqlSelectMhs = mysqli_fetch_assoc($sqlSelectMhs);
                $sqlSelectKelas = sqlSelect($connectingToDb, "*","kelas k"," INNER JOIN anggota_kelas ak ON ak.kelas_id=k.id
                                                                             WHERE ak.mahasiswa_id=".$sqlSelectMhs['id']);
                $sqlSelectKelas = mysqli_fetch_assoc($sqlSelectKelas);
                
                $_SESSION['users']['mahasiswa'] = $sqlSelectMhs;
                $_SESSION['kelas'] = $sqlSelectKelas;
                header("Location: "."../view/mahasiswa/dashboard.php");
            }
        }
    }
?>