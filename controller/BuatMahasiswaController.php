<?php
     include("../controller/auth.php");
     include("../controller/helper-func.php");
     session_start();

     $namaMahasiswa = $_POST['nama_mahasiswa'];
     $jenisKelamin = $_POST['jenis_kelamin'];
     $kota = $_POST['kota'];
     $nrp = $_POST['nrp'];
     $email = $_POST['email'];
     $password =  password_hash($_POST['password'], PASSWORD_DEFAULT);

    $sqlCheckEmail = sqlSelect($connectingToDb, "*", "users", "WHERE email='".$email."'");
    $sqlCheckEmail = mysqli_num_rows($sqlCheckEmail);
    if($sqlCheckEmail == 0) {
        $sqlinsert = "INSERT INTO users (email, password, type_user)
                      VALUES ('$email','$password','2')";
        $sqlinsert = mysqli_query($connectingToDb, $sqlinsert);
        $idUser = mysqli_insert_id($connectingToDb);
        if($idUser != 0) {
            $sqlCheckNrp = sqlSelect($connectingToDb, "*", "mahasiswa", "WHERE nrp='".$nrp."'");
            $sqlCheckNrp = mysqli_num_rows($sqlCheckNrp);
            if(!$sqlCheckNrp) {
                $sqlinsert = "INSERT INTO mahasiswa (user_id, nrp, kota_asal, jenis_kelamin, nama)
                              VALUES ('$idUser','$nrp','$kota','$jenisKelamin','$namaMahasiswa')";
                $sqlinsert = mysqli_query($connectingToDb, $sqlinsert);
                $checkIdMahasiswa = mysqli_insert_id($connectingToDb);
                if($checkIdMahasiswa != 0) {
                    $_SESSION['success_message'] = 'Berhasil membuat akun NetId '.$namaMahasiswa;
                    header('Location: '.$_SERVER['HTTP_REFERER']); 
                } else {
                    $_SESSION['error_message'] = 'Gagal membuat akun mahasiswa';
                    header('Location: '.$_SERVER['HTTP_REFERER']); 
                }
            } else {
                $_SESSION['error_message'] = 'Maaf NRP telah digunakan mahasiswa lain';
                header('Location: '.$_SERVER['HTTP_REFERER']);     
            }
        } else {
            $_SESSION['error_message'] = 'Gagal membuat akun NetId';
            header('Location: '.$_SERVER['HTTP_REFERER']); 
        }
    } else {
        $_SESSION['error_message'] = 'Maaf email NetId sudah digunakan';
        header('Location: '.$_SERVER['HTTP_REFERER']); 
    }
?>