<?php
    include("auth.php");
    $db = $connectingToDb;

    function getErrorMsg() {
        $errorMsg = $_SESSION['error_message'];
        unset($_SESSION['error_message']);
        return $errorMsg;
    }

    function getSuccessMsg() {
        $successMsg = $_SESSION['success_message'];
        unset($_SESSION['success_message']);
        return $successMsg;
    }

    function path($path_name) {
        return $_SERVER["DOCUMENT_ROOT"]."/project-akhir/controller/".$path_name;
    }
    
    function dd($var) {
        var_dump($var);
        die;
    }

    function sqlSelect($connection, $select_column, $table , $condition) {
        $sql = "SELECT ".$select_column." FROM ".$table." ".$condition;
        return mysqli_query($connection, $sql);
    }

    function getDosenNotJoinKelas($connection) {
        $query = sqlSelect($connection,"*","dosen","");
        $data = [];
        while($rows = mysqli_fetch_assoc($query)) {
            $queryCheck = sqlSelect($connection,"*","anggota_kelas","WHERE wali_kelas_id='".$rows['id']."'");
            if(!mysqli_fetch_assoc($queryCheck)) {
                $data[] = $rows;
            }
        }
        return $data; 
    }

    function getMahasiswaNotJoinKelas($connection) {
        $query = sqlSelect($connection,"*","mahasiswa","");
        $data = [];
        while($rows = mysqli_fetch_assoc($query)) {
            $queryCheck = sqlSelect($connection,"*","anggota_kelas","WHERE mahasiswa_id=".$rows['id']);
            if(!mysqli_fetch_assoc($queryCheck)) {
                $data[] = $rows;
            }
        }
        return $data;
    }
    
    function totalAnggotaKelas($connection,$kelasId) {
        $query = sqlSelect($connection,"*","anggota_kelas","WHERE kelas_id='".$kelasId."'");
        return mysqli_num_rows($query);
    }

    function getDataDosenByIdKelas($connection, $kelasId){
        $query = sqlSelect($connection,"*","anggota_kelas","WHERE kelas_id='".$kelasId."'");
        $dataKelas = mysqli_fetch_assoc($query);

        $query = sqlSelect($connection,"*","dosen","WHERE id='".$dataKelas['wali_kelas_id']."'");
        return mysqli_fetch_assoc($query);
    }

    function getDataAnggotaKelasByIdKelas($connection, $kelasId) {
        $query = sqlSelect($connection,"*","anggota_kelas","WHERE kelas_id='".$kelasId."'");
        return mysqli_fetch_assoc($query);
    }

    function getDataMahasiswa($connection, $mahasiswaId) {
        $query = sqlSelect($connection,"*","mahasiswa","WHERE id='".$mahasiswaId."'");
        return mysqli_fetch_assoc($query);
    }

    function sqlDelete($connection,$query) {
        return mysqli_query($connection, $query);
    }

    function getAllDosen($connection) {
        $sql = 'SELECT * FROM dosen';
        return mysqli_query($connection, $sql);
    }

    function sqlUpdate($connection, $table, $dataUpdate , $condition) {
        $sql = "UPDATE ".$table." SET ".$dataUpdate." ".$condition;
        return mysqli_query($connection, $sql); 
    }

    function convertBytes($bytes) {

        $totalBytes = ceil($bytes / 1000);
        if($totalBytes > 1000) {
          $totalBytes = $totalBytes / 1000 . " MB";
        } else {
          $totalBytes .= " KB";
        }
  
        return $totalBytes;
      }
?>