<?php 
     include("../controller/auth.php");
     include("../controller/helper-func.php");
     session_start();

     $user = $_SESSION['users'];
     if($user['type_user'] != 2) {
        $_SESSION['error_message'] = "Maaf kamu hanya bisa mengakses role mahasiswa";
        if($user['type_user'] == 1) {
         $user = 'dosen';
        } else {
         $user = 'admin';
        }
        header("location: ../../view/$user/dashboard.php");
     }
?>