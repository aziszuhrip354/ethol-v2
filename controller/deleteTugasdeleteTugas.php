<?php 
    include("../controller/auth.php");
    include("../controller/helper-func.php");
    session_start();
    
    $idTugas = $_POST['idTugas'];
    $namaTugas = $_POST['nama_tugas'];
    $sqlDelete = sqlDelete($connectingToDb, "DELETE FROM tugas WHERE id=". $idTugas);
    if($sqlDelete) {
        $_SESSION['success_message'] = 'Berhasil menghapus tugas '. $namaTugas;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else {
        $_SESSION['success_message'] = 'Gagal menghapus tugas '. $namaTugas;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
?>