<?php 
    include("../controller/auth.php");
    include("../controller/helper-func.php");
    session_start();

    $idUser = $_POST['idUser'];
    $idDosen = $_POST['idDosen'];
    $namaDosen = $_POST['nama_dosen'];
    $emailDosen = $_POST['email'];
    $gelarBelakang = $_POST['gelar_belakang'];
    $gelarDepan = $_POST['gelar_dpn'];
    $joinAt = $_POST['join_at'];
    $password = $_POST['password'];

    $sqlSelectUser = sqlSelect($connectingToDb, "*","users","WHERE id='".$idUser."'");
    $sqlSelectUser = mysqli_fetch_assoc($sqlSelectUser);
    $sqlSelectDosen = sqlSelect($connectingToDb, "*","dosen","WHERE id='".$idDosen."'");
    $sqlSelectDosen = mysqli_fetch_assoc($sqlSelectDosen);
    
    $checkingUserEmail = sqlSelect($connectingToDb, "*","users","WHERE email='".$emailDosen."' AND id!='$idUser'");
    $checkingUserEmail = mysqli_fetch_assoc($checkingUserEmail);

    if($checkingUserEmail) {
        $_SESSION['error_message'] = 'Maaf... email telah digunakan';
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else {
        if(!$sqlSelectUser) {
            $_SESSION['error_message'] = 'Maaf... data user tidak valid';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        } else {
            if(!$password) {
                $updateUser = sqlUpdate($connectingToDb, "users"," email='$emailDosen'"," WHERE id='$idUser'");
            } else {
                $password = password_hash($password, PASSWORD_DEFAULT);
                $updateUser = sqlUpdate($connectingToDb, "users"," email='$emailDosen', WHERE password='$password'"," WHERE id='$idUser'");
            }
            if(!$updateUser) {
                $_SESSION['error_message'] = 'Maaf... data user gagal diupdate';
                header('Location: '.$_SERVER['HTTP_REFERER']);
            }
        }

        if(!$sqlSelectDosen) {
            $_SESSION['error_message'] = 'Maaf... data dosen tidak valid';
            header('Location: '.$_SERVER['HTTP_REFERER']);
        } else {
            $updateDosen = sqlUpdate($connectingToDb, "dosen","nama='$namaDosen',gelar_depan='$gelarDepan',gelar_belakang='$gelarBelakang',join_at='$joinAt'"," WHERE id='$idDosen'");
            if(!$updateDosen) {
                $_SESSION['error_message'] = 'Maaf... data dosen gagal diupdate';
                header('Location: '.$_SERVER['HTTP_REFERER']);
            }
        }

        $_SESSION['success_message'] = 'Data dosen '.$namaDosen.' berhasil diupdate';
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
    

?>