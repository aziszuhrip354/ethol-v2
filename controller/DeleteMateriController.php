<?php 
     include("../controller/auth.php");
     include("../controller/helper-func.php");
     session_start();

     $idMateri = $_POST['idMateri'];
     $namaMateri = $_POST['nama_materi'];
     $sqlSelect = sqlSelect($connectingToDb, "*","materi","WHERE id=".$idMateri);
     $sqlSelect = mysqli_fetch_assoc($sqlSelect);
     if($sqlSelect) {
        $sqlDelete = "DELETE FROM materi WHERE id=".$idMateri;
        $sqlDelete = mysqli_query($connectingToDb, $sqlDelete);
        if($sqlDelete) {
            if(file_exists($sqlSelect['path'])) {
                unlink($sqlSelect['path']);
            }
            $_SESSION['success_message'] = 'Berhasil menghapus materi '.$sqlSelect['nama'];
            header("location: ".$_SERVER['HTTP_REFERER']);
        } else {
            $_SESSION['error_message'] = 'Gagal menghapus materi';
            header("location: ".$_SERVER['HTTP_REFERER']);
        }
     } else {
        $_SESSION['error_message'] = 'materi tidak ditemukan';
        header("location: ".$_SERVER['HTTP_REFERER']);
     }
?>