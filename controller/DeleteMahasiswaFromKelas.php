<?php
     include("../controller/auth.php");
     include("../controller/helper-func.php");
     session_start();

     $idKelas = $_POST['idKelas'];
     $idWalas = $_POST['idDosen'];
     $idMahasiswa = $_POST['idMahasiswa'];
     $namaMhs = $_POST['nama_mahasiswa'];
     $namaKelas = $_POST['nama_kelas'];


     $queryDelete = sqlDelete($connectingToDb,
                    "DELETE FROM anggota_kelas
                     WHERE mahasiswa_id='".$idMahasiswa."' AND kelas_id='".$idKelas."'"); 
     if($queryDelete) {
        $sqlCheck = sqlSelect($connectingToDb,"*","anggota_kelas","WHERE wali_kelas_id='".$idWalas."'");
        $totalDataAnggotaKelas = mysqli_num_rows($sqlCheck);
        if($totalDataAnggotaKelas == 0) {
            $sqlDelete = sqlDelete($connectingToDb,
                        "DELETE FROM kelas WHERE id='".$idKelas."'"); 
            if($sqlDelete) {
                $_SESSION['success_message'] = 'Berhasil menghapus '.$namaMhs." dari kelas ".$namaKelas." dan kelas otomatis terhapus";
                header('Location: '.$_SERVER['HTTP_REFERER']);
            }
        } else {
            $_SESSION['success_message'] = 'Berhasil menghapus '.$namaMhs." dari kelas ".$namaKelas;
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
     } else {
        $_SESSION['error_message'] = 'Maaf gagal mengapus mahasiswa dari kelas'.$namaKelas;
        header('Location: '.$_SERVER['HTTP_REFERER']);
     }
?>