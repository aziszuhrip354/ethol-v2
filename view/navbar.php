<nav class="navbar navbar-expand-lg main-navbar d-flex justify-content-end">
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <div class="d-sm-none d-lg-inline-block">Hi, <?= $_SESSION['users']['email']?>
            <?php if($_SESSION["users"]["type_user"] == 0) {?>
              <span class="font-weight-bold" style="color: #7cff8d;">(Admin BAAK)</span>
            <?php } else if($_SESSION["users"]["type_user"] == 1) {?>
              <span class="" style="color: #f5ff7c">(Dosen)</span>
            <?php } else{?>
              <span class="" style="color: #7cffda">(Mahasiswa)</span>
            <?php } ?>

          </div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-divider"></div> 
              <a href="javascript:void(0)" onclick="logoutUser('formLogout');" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
              <form action="../../controller/logout.php" method="POST" id="formLogout">
              </form>
            </div>
          </li>
        </ul>
      </nav>
    <script>
      function logoutUser(idForm) {
          let confirmation = confirm('Apakah Anda ingin logout');
          if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
          }
      }
</script>