<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include("../../controller/middleware-admin.php");
    include('../../controller/helper-func.php');

    $querySelectDosen = sqlSelect($connectingToDb,"*","users","WHERE type_user=1");
    $getDataTotalDosen = mysqli_num_rows($querySelectDosen);
    $querySelectMahasiswa = sqlSelect($connectingToDb,"*","users","WHERE type_user=2");
    $getDataTotalMahasiswa = mysqli_num_rows($querySelectMahasiswa);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content" style="min-height: 838px;">
        <section class="section">
            <div class="section-header">
                <h1>Buat Kelas</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item">Kelas</div>
                    <div class="breadcrumb-item">Buat Kelas</div>
                </div>
            </div>
            
          <div class="section-body">
            <h2 class="section-title">Form Buat Kelas</h2>
            <p class="section-lead">Tempat pembuat kelas</p>

            <div class="row">
              <div class="col-12">
                  <form class="card" id="formBuatKelas" method="POST" action="../../controller/BuatKelasController.php">
                    <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nama Kelas</label>
                      <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control phone-number" name="nama_kelas" required>
                        </div>
                    </div>
                    <div class="form-group">
                      <label>Tahun Angkatan</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control phone-number" name="angkatan" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Wali Kelas</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                  </div>
                                </div>
                                <select class="form-control" name="wali_kelas" required>
                                    <option value="">-- Pilih Wali Kelas --</option>
                                    <?php foreach(getDosenNotJoinKelas($connectingToDb) as $data) { ?>
                                        <?php
                                            $gelarBelakang = str_replace(",",", ", $data['gelar_belakang']);
                                            $gelarDpn = str_replace(",",", ", $data['gelar_depan']);
                                        ?>
                                        <option value="<?= $data['id'] ?>"><?= $gelarDpn.$data['nama'].", ".$gelarBelakang ?></option>
                                    <?php } ?>
                                </select>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label> Cari Mahasiswa</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-search"></i>
                                  </div>
                                </div>
                                <input type="text" class="form-control" id="searching-mahasiswa">
                            </div>
                            <div class="border p-2" style="display: none;" id="dropdown-searching">
                                <div class="text-center text-primary">Data tidak ada</div>
                                <!-- <div class="border p-2 text-primary text-center">3121600002 - Azis Zuhri Pratomo</div> -->
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col">
                              <span>Daftar Anggota Kelas :  </span>
                              <ul id="list-anggota-kelas">
                                  <li id="member-null">Masih Kosong</li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <button class="btn btn-primary">Buat Kelas</button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      let DataMahasiswa = '<?= json_encode(getMahasiswaNotJoinKelas($connectingToDb)) ?>';
      let mahasiswa = JSON.parse(DataMahasiswa);;
      console.log(mahasiswa);

      $("#searching-mahasiswa").on("keyup", () => {
        $("#dropdown-searching").show().delay(300).fadeIn();

        let dataJson = [];
        let data = $("#searching-mahasiswa").val();
        let defaultEl = `<div class="text-center text-primary">Data tidak ada</div>`;
        let elNested = '';        
        
        mahasiswa.forEach( val => {
            let nama = val.nama.toUpperCase();
            let checkingDocumentById = document.getElementById('input'+val.nrp);
            if(data != '') {
                if( ((nama.includes(data.toUpperCase()) || val.nrp.includes(data)) && checkingDocumentById == null)) {
                    let el = `<div class="border p-2 text-primary text-center" onclick="enterToListClass(this, '${val.nama}','${val.nrp}','${val.id}')">${val.nrp} - ${val.nama}</div>`;
                    elNested += el;
                }
            }   
        });

        if(elNested != '') {
            $("#dropdown-searching").html(elNested);
        } else {
            $("#dropdown-searching").html(defaultEl);
            setTimeout(() => {
                $("#dropdown-searching").hide().delay(300).fadeOut();
            }, 3000);
        }  
      })

      enterToListClass = (self, nama, nrp, code_id) => {
          $("#member-null").hide();
          $("#searching-mahasiswa").val('');
          $(self).remove();
          let el = `<li id="mahasiswa${nrp}"> <input id="input${nrp}" type='hidden' name="idMember[]" value="${code_id}">${nama} - ${nrp}</li>`;

          $('#list-anggota-kelas').append(el);
      }

      function logout(idForm) {
          let confirmation = confirm('Apakah Anda ingin logout');
          if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
          }
      }

      function  delContact(idForm, contactName) {
        let confirmation = confirm('Apakah Anda ingin menghapus kontak '+contactName+" ?");
        if(confirmation) {
          let form = document.getElementById(idForm);
          form.submit();
        }
      }
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
