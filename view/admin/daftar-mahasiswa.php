<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");


    $querySelectMahasiswa = sqlSelect($connectingToDb,"*","users","WHERE type_user=2");
    $getDataTotalMahasiswa = mysqli_num_rows($querySelectMahasiswa);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>Daftar Mahasiswa</h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                  <div class="breadcrumb-item">Data Mahasiswa</div>
                  <div class="breadcrumb-item">Daftar Mahasiswa</div>
                </div>
              </div>
              <secion class="section">
              </secion>
              <div class="section-body">
                <h2 class="section-title">Daftar Kelas</h2>
                <p class="section-lead">Halaman yang menyajikan data keseluruhan tentang mahasiswa</p>
                <div class="card">
                  <div class="row mt-4">
                  <div class="col-12">
                  <div class="">
                  <div class="row mt-4">
                  <div class="col-12">
                    <div class="">
                      <div class="card-body">
                        <div class="table-responsive">
                        <?php if($_SESSION['error_message']) {?>
                            <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                                <?= getErrorMsg() ?>
                                </div>
                            <?php }?>
                            <?php if($_SESSION['success_message']) {?>
                            <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                                <?= getSuccessMsg() ?>
                                </div>
                            <?php }?>
                          <a href="buat-mahasiswa.php" class="btn btn-success my-4 ml-2">Buat Mahasiswa</a>

                          <table class="table table-striped">
                            <tbody><tr>
                              <th>NRP</th>
                              <th class="text-center">Nama</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">Kota Asal</th>
                              <th class="text-center">Jenis Kelamin</th>
                              <th></th>
                            </tr>
                            <?php if($getDataTotalMahasiswa > 0) { ?>
                              <?php while($rows = mysqli_fetch_assoc($querySelectMahasiswa)) {?>
                                <?php 
                                  $data = sqlSelect($connectingToDb, "*", "mahasiswa","WHERE user_id=".$rows['id']);
                                  $data = mysqli_fetch_assoc($data);
                                ?>
                                <tr>
                                  <td>
                                    <?= $data['nrp'] ?>
                                  </td>
                                  <td class="text-center"><?= $data['nama'] ?></td>
                                  <td class="text-center"><?= $rows['email'] ?></td>
                                  <td class="text-center">
                                    <?= $data['kota_asal'] ?>
                                  </td>
                                  <td class="text-center"> <?= $data['jenis_kelamin'] ?></td>
                                  <td>
                                  <div class="table-links">
                                      <a href="javascript:void(0)" onclick="openModalEdit('<?= $rows['id'] ?>','<?= $data['id'] ?>','<?= $data['nama']?>','<?= $rows['email'] ?>','<?= $data['jenis_kelamin'] ?>','<?= $data['kota_asal'] ?>','<?=$data['nrp']?>')">Edit</a>
                                      <div class="bullet"></div>
                                      <a href="javascript:void(0);" onclick="deleteMahasiswa('<?= $rows['rows']?>','<?= $data['nama'] ?>','formHapusMahasiswa<?= $rows['id'] ?>')" class="text-danger">Hapus</a>
                                      <form action="../../controller/DeleteMahasiswa.php" method="POST" id="formHapusMahasiswa<?= $rows['id'] ?>">
                                        <input type="hidden" name="idUser" value="<?=$rows['id']?>">
                                        <input type="hidden" name="namaMahasiswa" value="<?=$data['nama']?>">
                                    </form>
                                    </div>
                                  </td>
                                </tr>
                              <?php }?>
                            <?php } else {?>
                              <tr><td colspan="6" class="text-center fw-bold font-italic">Data kontak kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
                </div>
              </div>
            </section>
          </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Edit Data Dosen </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="../../controller/UpdateMahasiswa.php" method="POST" id="formEdit">
                <input type="hidden" name="idUser">
                <input type="hidden" name="idMahasiswa">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Mahasiswa </label>
                    <input type="text" id="nama_dosen" name="nama_mahasiswa"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama dosen..">
                </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">NRP</label>
                  <input type="text" id="gelar_dpn" name="nrp"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan gelar depan dosen">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Jenis Kelamin</label>
                  <select id="jenis_kelamin" name="jenis_kelamin"  class="form-control">
                      <option value="L">L</option>
                      <option value="P">P</option>
                  </select>
              </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email </label>
                    <input type="text" id="email" name="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan gelar belakang dosen">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Kota</label>
                    <input type="text" id="kota" name="kota"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan data kota">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Ganti Password</label>
                    <input type="password" id="password" name="password"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan password jika ingin ganti password....">
              </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" id="submitEditBtn" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </div>
    </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
       deleteMahasiswa = (id, nama_mahasiswa, idForm) => {
        let confirmation = confirm(`Apakah Anda yakin ingin menghapus mahasiswa ${nama_mahasiswa} ?`);
        if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
        }
      }

      openModalEdit = (idUser, idMahasiswa, nama, email, jenis_kelamin, kota, nrp) => {
        $("#exampleModalLabel").text(`Edit Data Mahasiswa ${nama}`)
        $("#exampleModal").modal("show");
        $("[name='nama_mahasiswa']").val(nama);
        $("[name='idUser']").val(idUser);
        $("[name='idMahasiswa']").val(idMahasiswa);
        $("[name='email']").val(email);
        $("[name='jenis_kelamin']").val(jenis_kelamin);
        $("[name='kota']").val(kota);
        $("[name='nrp']").val(nrp);
      }

      $("#submitEditBtn").on("click", () => {
        let namaMahasiswa = $("[name='nama_dosen']").val();
        let emailMahasiswa = $("[name='email']").val();
        let jenisKelamin = $("[name='gelar_belakang']").val();
        let kota = $("[name='join_at']").val();
        let nrp = $("[name='nrp']").val();
        if(namaMahasiswa == '') {
            alert("Maaf nama mahasiswa tidak boleh kosong");
        } else if(emailMahasiswa == '') {
            alert("Maaf email mahasiswa tidak boleh kosong");
        } else if(jenisKelamin == '') {
            alert("Maaf jenis kelamin tidak boleh kosong");
        } else if(nrp == '') {
            alert("Maaf nrp mahasiswa tidak boleh kosong");
        } else if(kota == '') {
            alert("Maaf data kota mahasiswa tidak boleh kosong");
        } else {
            let form = document.getElementById('formEdit');
            form.submit();
        }
      });
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
