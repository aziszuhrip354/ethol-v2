<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");


    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","");
    $getDataTotalKelas = mysqli_num_rows($querySelectKelas);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>Daftar Kelas</h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                  <div class="breadcrumb-item">Kelas</div>
                  <div class="breadcrumb-item">Daftar Kelas</div>
                </div>
              </div>
              <secion class="section">
              </secion>
              <div class="section-body">
                <h2 class="section-title">Daftar Kelas</h2>
                <p class="section-lead">Halaman yang menyajikan data keseluruhan tentang kelas</p>
                <div class="">
                  <div class="row mt-4">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-body">
                        <?php if($_SESSION['error_message']) {?>
                                <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                                    <?= getErrorMsg() ?>
                                    </div>
                                <?php }?>
                                <?php if($_SESSION['success_message']) {?>
                                <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                                    <?= getSuccessMsg() ?>
                                    </div>
                                <?php }?>
                        <div class="table-responsive">
                          <a href="buat-kelas.php" class="btn btn-success my-4 ml-2">Buat Kelas</a>

                          <table class="table table-striped">
                            <tbody><tr>
                              <th>Nama Kelas</th>
                              <th class="text-center">Angkatan</th>
                              <th class="text-center">Total Siswa</th>
                              <th></th>
                            </tr>
                            <?php if($getDataTotalKelas > 0) { ?>
                              <div class="accordion" id="accordionExample">
                                    <?php while($rows = mysqli_fetch_assoc($querySelectKelas)) {?>
                                        <?php
                                            $dataDosen = getDataDosenByIdKelas($connectingToDb, $rows['id']);
                                            $gelar_belakang = str_replace(',',", ", $dataDosen['gelar_belakang']);
                                            $dosen = $dataDosen['gelar_depan'].$dataDosen['nama'].", ".$gelar_belakang;
                                            $idKelas = $rows['id'];
                                            $namaKelas = $rows['nama_kelas'];
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $rows['nama_kelas'] ?>
                                            </td>
                                            <td class="text-center"><?= $rows['angkatan'] ?></td>
                                            <td class="text-center">
                                                <?= totalAnggotaKelas($connectingToDb, $rows['id']) ?>
                                            </td>
                                            <td>
                                            <div class="table-links">
                                                <div class="bullet"></div>
                                                <a href="javascript:void(0);"  onclick="openModalFormAdd('<?= $rows['id'] ?>','<?= $rows['nama_kelas'] ?>','<?=$dataDosen['id']?>');" class="text-success">Tambah Mahasiswa</a>
                                                <div class="bullet"></div>
                                                <a href="javascript:void(0);" onclick="deleteClass('<?= $rows['id'] ?>','<?= $rows['nama_kelas'] ?>','DeleteKelas<?= $rows['id'] ?>');" class="text-danger">Hapus Kelas</a>
                                                <div class="bullet"></div>
                                                <a href="daftar-mata-kuliah.php?kode_kelas=<?= base64_encode($rows['id']) ?>" class="text-info">Mata kuliah <?= $rows['nama_kelas'] ?></a>
                                                </div>
                                                <form action="../../controller/DeleteKelas.php" method="POST" id='DeleteKelas<?= $rows['id'] ?>'>
                                                    <input type="hidden" name="idKelas" value="<?= $rows['id'] ?>">
                                                    <input type="hidden" name="namaKelas" value="<?= $rows['nama_kelas'] ?>">
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div class="">
                                                    <div class="card-header" id="headingOne">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $rows['id'] ?>" aria-expanded="false" aria-controls="collapse<?= $rows['id'] ?>">
                                                                Detail Kelas <?= $rows['nama_kelas']?>
                                                            </button>
                                                        </h2>
                                                    </div>

                                                    <div id="collapse<?= $rows['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        
                                                        <div>Wali Kelas : <?= $dosen ?> </div>
                                                        <div>Anggota Kelas: </div>
                                                        <ul class="p-2">
                                                            <?php if(totalAnggotaKelas($connectingToDb, $rows['id']) != 0) { ?>
                                                                <?php
                                                                    $counter = 1;
                                                                    $sqlMahasiswa = sqlSelect($connectingToDb,"*", "anggota_kelas","WHERE kelas_id='".$rows['id']."'");
                                                                ?>
                                                                <?php while($rows = mysqli_fetch_assoc($sqlMahasiswa)) {?>
                                                                     <?php
                                                                        $dataMahasiswa = getDataMahasiswa($connectingToDb, $rows['mahasiswa_id']);
                                                                     ?>
                                                                    <li class="my-2 d-flex">
                                                                        <span class="d-flex flex-column justify-content-center align-item-center w-50" width="50" class="w-50"><?= $counter++ .". ". $dataMahasiswa['nama'] ?> - <?= $dataMahasiswa['nrp'] ?></span>
                                                                        <button class="btn btn-danger" onclick="deleteFromClass(this,'<?= $namaKelas ?>', '<?= $dataMahasiswa['nama'] ?>','hapusMahasiswa<?= $dataMahasiswa['nrp']?>')">Hapus</button>
                                                                        <form action="../../controller/DeleteMahasiswaFromKelas.php" method="POST" class="d-none" id="hapusMahasiswa<?= $dataMahasiswa['nrp'] ?>">
                                                                            <input type="hidden" name="idKelas" value="<?= $idKelas ?>">
                                                                            <input type="hidden" name="idDosen" value="<?= $dataDosen['id'] ?>">
                                                                            <input type="hidden" name="nama_mahasiswa" value="<?= $dataMahasiswa['nama'] ?>">
                                                                            <input type="hidden" name="nama_kelas" value="<?= $namaKelas ?>">
                                                                            <input type="hidden" name="idMahasiswa" value="<?= $rows['mahasiswa_id']?>">
                                                                        </form>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <li>Data kosong</li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        
                                    <?php }?>
                              </div>
                            <?php } else {?>
                              <tr><td colspan="4" class="text-center fw-bold font-italic">Data kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                        <!-- <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Collapsible Group Item #1
                                    </button>
                                </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Collapsible Group Item #2
                                    </button>
                                </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Collapsible Group Item #3
                                    </button>
                                </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                                </div>
                            </div>
                            </div>
                      </div> -->
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </section>
          </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Tambah Data Mahasiswa Ke Kelas </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="../../controller/UpdateAnggotaKelas.php" method="POST" id="formAddMahasiswaToKelas">
                <div class="form-group">
                    <label for="exampleInputEmail1">Cari Mahasiswa</label>
                    <input type="text" id="searching-mahasiswa"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama atau NRP">
                    <div class="border p-2" style="display: none;" id="dropdown-searching">
                        <div class="text-center text-primary">Data tidak ada</div>
                        <!-- <div class="border p-2 text-primary text-center">3121600002 - Azis Zuhri Pratomo</div> -->
                    </div>
                    <br>
                    <input type="hidden" name="wali_kelas_id">
                    <input type="hidden" name="kelas_id">
                    <input type="hidden" name="nama_kelas">
                    <span id="exampleModalLabel2">Daftar Anggota Kelas Baru :  </span>
                    <ul class="pl-3" id="list-anggota-kelas">
                        <li id="member-null">Masih Kosong</li>
                    </ul>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" id="submitBtnAddMahasiswaKelas" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </div>
    </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      function logout(idForm) {
          let confirmation = confirm('Apakah Anda ingin logout');
          if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
          }
      }

      $("#submitBtnAddMahasiswaKelas").on("click", () => {
        let form = document.getElementById("formAddMahasiswaToKelas");
        form.submit();
      });

      function  delContact(idForm, contactName) {
        let confirmation = confirm('Apakah Anda ingin menghapus kontak '+contactName+" ?");
        if(confirmation) {
          let form = document.getElementById(idForm);
          form.submit();
        }
      }

      openModalFormAdd = (idKelas, nama_kelas, walasId) => {
          $("#searching-mahasiswa").val("");
          $("#exampleModal").modal('show');
          $("[name='wali_kelas_id']").val(walasId);
          $("[name='nama_kelas']").val(nama_kelas);
          $("[name='kelas_id']").val(idKelas);
          $("#exampleModalLabel").text(`Tambah Data Mahasiswa Ke Kelas ${nama_kelas}`)
          $("#exampleModalLabel2").text(`Daftar Anggota Baru Kelas ${nama_kelas}: `)
      }

      let DataMahasiswa = '<?= json_encode(getMahasiswaNotJoinKelas($connectingToDb)) ?>';
      let mahasiswa = JSON.parse(DataMahasiswa);

      $("#searching-mahasiswa").on("keyup", () => {
        $("#dropdown-searching").show().delay(300).fadeIn();

        let dataJson = [];
        let data = $("#searching-mahasiswa").val();
        let defaultEl = `<div class="text-center text-primary">Data tidak ada</div>`;
        let elNested = '';        
        
        mahasiswa.forEach( val => {
            let nama = val.nama.toUpperCase();
            let checkingDocumentById = document.getElementById('input'+val.nrp);
            if(data != '') {
                if( ((nama.includes(data.toUpperCase()) || val.nrp.includes(data)) && checkingDocumentById == null)) {
                    let el = `<div class="border p-2 text-primary text-center" onclick="enterToListClass(this, '${val.nama}','${val.nrp}','${val.id}')">${val.nrp} - ${val.nama}</div>`;
                    elNested += el;
                }
            }   
        });

        if(elNested != '') {
            $("#dropdown-searching").html(elNested);
        } else {
            $("#dropdown-searching").html(defaultEl);
            setTimeout(() => {
                $("#dropdown-searching").hide().delay(300).fadeOut();
            }, 3000);
        }  
      })

      enterToListClass = (self, nama, nrp, code_id) => {
          $("#member-null").hide();
          $("#searching-mahasiswa").val('');
          $(self).remove();
          let el = `<li id="mahasiswa${nrp}"> <input id="input${nrp}" type='hidden' name="idMember[]" value="${code_id}">${nama} - ${nrp}</li>`;

          $('#list-anggota-kelas').append(el);
      }

      deleteFromClass = (self, nama_kelas, nama, idForm) => {
        let confirmation = confirm(`Apakah Anda yakin ingin menghapus ${nama} dari kelas ${nama_kelas} ?`);
        if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
        }
      }

      deleteClass = (id, nama_kelas, idForm) => {
        let confirmation = confirm(`Apakah Anda yakin ingin menghapus kelas ${nama_kelas} ?`);
        if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
        }
      }
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
