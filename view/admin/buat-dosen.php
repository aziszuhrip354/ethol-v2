<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");

    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","");
    $getDataTotalKelas = mysqli_num_rows($querySelectKelas);


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content" style="min-height: 838px;">
        <section class="section">
            <div class="section-header">
                <h1>Buat Dosen</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item">Data Dosen</div>
                    <div class="breadcrumb-item">Buat Dosen</div>
                </div>
            </div>
            
          <div class="section-body">
            <h2 class="section-title">Form Buat Dosen</h2>
            <p class="section-lead">Tempat pembuat akun NetId Dosen</p>

            <div class="row">
              <div class="col-12">
                  <form class="card" id="formBuatKelas" method="POST" action="../../controller/BuatDosenController.php">
                    <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nama Dosen</label>
                      <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control phone-number" name="nama_kelas" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Gelar Depan</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                  </div>
                                </div>
                                <input type="text" class="form-control" name="gelar_dpn">
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label> Gelar Belakang</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                  </div>
                                </div>
                                <input type="text" class="form-control" id="" name="gelar_belakang">
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Email</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                  </div>
                                </div>
                                <input type="text" class="form-control" id="" name="email">
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Password</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">
                                    <i class="fas fa-star"></i>
                                  </div>
                                </div>
                                <input type="password" class="form-control" id="" name="password" required>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Tahun Bergabung</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control phone-number" name="join_at" required>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <button class="p-2 btn btn-primary m-4">Buat Dosen</button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Tambah Data Mahasiswa Ke Kelas </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Cari Mahasiswa</label>
                    <input type="text" id="searching-mahasiswa"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <div class="border p-2" style="display: none;" id="dropdown-searching">
                        <div class="text-center text-primary">Data tidak ada</div>
                        <!-- <div class="border p-2 text-primary text-center">3121600002 - Azis Zuhri Pratomo</div> -->
                    </div>
                    <br>
                    <span id="exampleModalLabel2">Daftar Anggota Kelas Baru :  </span>
                    <ul class="pl-3" id="list-anggota-kelas">
                        <li id="member-null">Masih Kosong</li>
                    </ul>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </div>
    </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
    
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
