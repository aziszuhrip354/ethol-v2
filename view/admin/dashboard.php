<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");


    $querySelectDosen = sqlSelect($connectingToDb,"*","users","WHERE type_user=1");
    $getDataTotalDosen = mysqli_num_rows($querySelectDosen);
    $querySelectMahasiswa = sqlSelect($connectingToDb,"*","users","WHERE type_user=2");
    $getDataTotalMahasiswa = mysqli_num_rows($querySelectMahasiswa);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","");
    $getDataTotalKelas = mysqli_num_rows($querySelectKelas);
    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","");
    $getDataTotalMatkul = mysqli_num_rows($querySelectMatkul);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <?php if($_SESSION['users']['type_user'] == 0) {?>
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>Dashboard</h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                </div>
              </div>
              <secion class="section">
              <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                  <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                      <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                      <div class="card-header">
                        <h4>Total Dosen</h4>
                      </div>
                      <div class="card-body">
                        <?= $getDataTotalDosen ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                  <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                      <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                      <div class="card-header">
                        <h4>Mahasiswa</h4>
                      </div>
                      <div class="card-body">
                      <?= $getDataTotalMahasiswa ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                  <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                      <!-- <i class="far fa-pencil"></i> -->
                      <i class="fas fa-pen"></i>
                    </div>
                    <div class="card-wrap">
                      <div class="card-header">
                        <h4>Kelas</h4>
                      </div>
                      <div class="card-body">
                        <?= $getDataTotalKelas?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                  <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                      <i class="fas fa-circle"></i>
                    </div>
                    <div class="card-wrap">
                      <div class="card-header">
                        <h4>Mata Kuliah</h4>
                      </div>
                      <div class="card-body">
                        <?= $getDataTotalMatkul ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </secion>
              <div class="section-body">
                <h2 class="section-title">Halaman Dashboard</h2>
                <p class="section-lead">Halaman yang menyajikan statistik data keseluruhan</p>
                <div class="card">
                  <div class="row mt-4">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-header">
                          <h4>Data Dosen</h4>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-striped">
                            <tbody><tr>
                              <th>Nama</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">Gabung Sejak</th>
                            </tr>
                            <?php if($getDataTotalDosen > 0) { ?>
                              <?php while($rows = mysqli_fetch_assoc($querySelectDosen)) {?>
                                <?php 
                                  $data = sqlSelect($connectingToDb, "*", "dosen","WHERE user_id=".$rows['id']);
                                  $data = mysqli_fetch_assoc($data);
                                  $gelarBelakang = str_replace(",",", ", $data['gelar_belakang']);
                                  $gelarDpn = str_replace(",",", ", $data['gelar_depan']);
                                ?>
                                <tr>
                                  <td>
                                    <?= $gelarDpn.$data['nama'].", ".$gelarBelakang ?>
                                  </td>
                                  <td class="text-center"><?= $rows['email'] ?></td>
                                  <td class="text-center">
                                    <?= Date("d-m-Y", strtotime($data['join_at'])) ?>
                                  </td>
                                </tr>
                              <?php }?>
                            <?php } else {?>
                              <tr><td colspan="3" class="text-center fw-bold font-italic">Data kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                        <div class="card-header">
                          <h4>Data Mahasiswa</h4>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-striped">
                            <tbody><tr>
                              <th>NRP</th>
                              <th class="text-center">Nama</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">Kota Asal</th>
                              <th class="text-center">Jenis Kelamin</th>
                            </tr>
                            <?php if($getDataTotalMahasiswa > 0) { ?>
                              <?php while($rows = mysqli_fetch_assoc($querySelectMahasiswa)) {?>
                                <?php 
                                  $data = sqlSelect($connectingToDb, "*", "mahasiswa","WHERE user_id=".$rows['id']);
                                  $data = mysqli_fetch_assoc($data);
                                ?>
                                <tr>
                                  <td>
                                    <?= $data['nrp'] ?>
                                  </td>
                                  <td class="text-center"><?= $data['nama'] ?></td>
                                  <td class="text-center"><?= $rows['email'] ?></td>
                                  <td class="text-center">
                                    <?= $data['kota_asal'] ?>
                                  </td>
                                  <td class="text-center"> <?= $data['jenis_kelamin'] ?></td>
                                 
                                </tr>
                              <?php }?>
                            <?php } else {?>
                              <tr><td colspan="4" class="text-center fw-bold font-italic">Data kontak kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                        <div class="float-right">
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </section>
          </div>
      <?php } else if($_SESSION['users']['type_user'] == 1) {?>

      <?php } else {?>
        
      <?php } ?>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      function logout(idForm) {
          let confirmation = confirm('Apakah Anda ingin logout');
          if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
          }
      }

      function  delContact(idForm, contactName) {
        let confirmation = confirm('Apakah Anda ingin menghapus kontak '+contactName+" ?");
        if(confirmation) {
          let form = document.getElementById(idForm);
          form.submit();
        }
      }
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
