<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");


    $base64KelasId = base64_decode($_GET['kode_kelas']);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","WHERE id='".$base64KelasId."'");
    $getDataDataKelas = mysqli_fetch_assoc($querySelectKelas);

    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE kelas_id='".$base64KelasId."'");
    $getDataTotalMakul = mysqli_num_rows($querySelectMatkul);

    $getDosen = getAllDosen($connectingToDb);
    $getTotalDosen = mysqli_num_rows($getDosen);
    
    $counterData = 1;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>Daftar Mata Kuliah <?= $getDataDataKelas['nama_kelas'] ?></h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                  <div class="breadcrumb-item active"> <a href="../admin/daftar-kelas.php">Kelas</a> </div>
                  <div class="breadcrumb-item active"> <a href="../admin/daftar-kelas.php">Daftar Kelas</a> </div>
                  <div class="breadcrumb-item"> Daftar Mata Kuliah <?= $getDataDataKelas['nama_kelas'] ?></div>
                </div>
              </div>
              <secion class="section">
              </secion>
              <div class="section-body">
                <h2 class="section-title">Daftar Matakuliah </h2>
                <p class="section-lead">Halaman yang menyajikan data mata kuliah kelas <?= $getDataDataKelas['nama_kelas'] ?></p>
                <div class="card">
                      <div class="card-body">
                        <div class="table-responsive">
                          <button id="btnOpenModal" class="btn btn-success my-3">Tambah Matkul</button>
                          <?php if($_SESSION['error_message']) {?>
                            <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                                <?= getErrorMsg() ?>
                                </div>
                            <?php }?>
                            <?php if($_SESSION['success_message']) {?>
                            <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                                <?= getSuccessMsg() ?>
                                </div>
                            <?php }?>
                          <table class="table table-striped">
                            <tbody><tr>
                              <th>#</th>
                              <th class="text-center">Nama Matkul</th>
                              <th class="text-center">Dosen Pengajar</th>
                              <th class="text-center">Waktu</th>
                              <th></th>
                            </tr>
                            <?php if($getDataTotalMakul > 0) { ?>
                              <?php while($rows = mysqli_fetch_assoc($querySelectMatkul)) {?>
                                <?php 
                                  $data = sqlSelect($connectingToDb, "*", "dosen","WHERE id=".$rows['dosen_id']);
                                  $data = mysqli_fetch_assoc($data);
                                  $gelarBelakang = str_replace(",",", ", $data['gelar_belakang']);
                                  $gelarDpn = str_replace(",",", ", $data['gelar_depan']);
                                ?>
                                <tr>
                                    <td><?= $counterData++ ?></td>
                                    <td class="text-center"><?= $rows['nama_matkul'] ?></td>
                                    <td class="text-center">
                                        <?= $gelarDpn.$data['nama'].", ".$gelarBelakang ?>
                                    </td>
                                    <td class="text-center"><?=$rows['jam_kelas']?></td>
                                    <td>
                                            <div class="table-links">
                                                <!-- <div class="bullet"></div>
                                                <a href="#">Edit</a> -->
                                                <div class="bullet"></div>
                                                <a href="javascript:void(0);" onclick="deleteMatkul('<?=$rows['id']?>','<?= $rows['nama_matkul'] ?>','deleteform<?= $rows['id'] ?>')" class="text-danger">Hapus Matkul</a>
                                                <form action="../../controller/DeleteMatkul.php" method="POST" id="deleteform<?= $rows['id'] ?>" class="d-none">
                                                    <input type="hidden" name="idMatkul" value="<?= $rows['id'] ?>">
                                                    <input type="hidden" name="namaMatkul" value="<?= $rows['nama_matkul'] ?>">
                                                </form>
                                            </div>
                                    </td>
                                </tr>
                              <?php }?>
                            <?php } else {?>
                              <tr><td colspan="5" class="text-center fw-bold font-italic">Data kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </section>
          </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Tambah Mata Kuliah <?= $getDataDataKelas['nama_kelas'] ?> </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="../../controller/TambahMatkul.php" id="formMatkul">
                <input type="hidden" name="idKelas" value="<?= $getDataDataKelas['id'] ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Matakuliah</label>
                    <input required type="text" name="nama_matkul" id="nama_matkul"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama matkul">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Dosen Pengampu</label>
                    <select name="dosen" class="form-control"  id="option_matkul" required>
                        <option value="null">-- Pilih Dosen Pengampu -- </option>
                        <?php while($rows = mysqli_fetch_assoc($getDosen)) {?>
                            <?php 
                                $gelarBelakang = str_replace(",",", ", $rows['gelar_belakang']);
                                $gelarDpn = str_replace(",",", ", $rows['gelar_depan']);
                            ?>
                            <option value="<?= $rows['id'] ?>"><?= $gelarDpn.$rows['nama'].", ".$gelarBelakang?></option>
                        <?php }?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Jadwal Matkul</label>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <label for="">Hari</label>
                                <select name="hari" id="selectHari" class="form-control">
                                    <option value="null">-- Pilih Hari --</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="">Jam Mulai</label>
                                <input type="time" id="jamStart" name="jamStart" class="form-control">
                            </div>
                            <div class="col">
                                <label for="">Jam Berakhir</label>
                                <input type="time" id="jamEnd" name="jamEnd" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" id="submitAddMatkul" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      $("#btnOpenModal").on("click", () => {
        $("#exampleModal").modal("show");
      });

      $("#submitAddMatkul").on("click",() => {
         let form = document.getElementById("formMatkul");
         let inputMatkul = $("#nama_matkul").val();
         let inputDosen = $("#option_matkul").val();
         let inputHari = $("#selectHari").val();
         let inputJamStart = $("#jamStart").val();
         let inputJamEnd = $("#jamEnd").val();
         if(inputDosen == 'null') {
            alert("Silahkan isi data dosen pengampu matakuliah");
         } else if(inputMatkul == '') {
            alert("Silahkan isi nama matakuliah");
         } else if(inputHari == 'null') {
            alert("Silahkan isi jadwal hari matakuliah");
         } else if(inputJamStart == '') {
            alert("Silahkan isi jadwal jam mulai matakuliah");
         } else if(inputJamEnd == '') {
            alert("Silahkan isi jadwal jam berakhir matakuliah");
         } else {
             form.submit();
         }
         
      });

      deleteMatkul = (id, nama_matkul, idForm) => {
        let confirmation = confirm(`Apakah Anda yakin ingin menghapus ${nama_matkul} ?`);
        if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
        }
      }
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
