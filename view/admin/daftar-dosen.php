<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-admin.php");

    $querySelectDosen = sqlSelect($connectingToDb,"*","users","WHERE type_user=1");
    $getDataTotalDosen = mysqli_num_rows($querySelectDosen);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
          <div class="main-content">
            <section class="section">
              <div class="section-header">
                <h1>Daftar Dosen</h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                  <div class="breadcrumb-item">Data Mahasiswa</div>
                  <div class="breadcrumb-item">Daftar Mahasiswa</div>
                </div>
              </div>
              <secion class="section">
              </secion>
              <div class="section-body">
                <h2 class="section-title">Daftar Dosen</h2>
                <p class="section-lead">Halaman yang menyajikan data keseluruhan tentang dosen</p>
                <div class="card">
                  <div class="row mt-4">
                  <div class="col-12">
                  <div class="">
                  <div class="row mt-4">
                  <div class="col-12">
                    <div class="">
                      <div class="card-body">
                        <div class="table-responsive">
                        <?php if($_SESSION['error_message']) {?>
                            <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                                <?= getErrorMsg() ?>
                                </div>
                            <?php }?>
                            <?php if($_SESSION['success_message']) {?>
                            <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                                <?= getSuccessMsg() ?>
                                </div>
                            <?php }?>
                          <a href="buat-dosen.php" class="btn btn-success my-4 ml-2">Buat Dosen</a>
                          <table class="table table-striped">
                            <tbody><tr>
                              <th>Nama</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">Gabung Sejak</th>
                              <th class="text-center">Status</th>
                              <th></th>
                            </tr>
                            <?php if($getDataTotalDosen > 0) { ?>
                              <?php while($rows = mysqli_fetch_assoc($querySelectDosen)) {?>
                                <?php 
                                  $data = sqlSelect($connectingToDb, "*", "dosen","WHERE user_id=".$rows['id']);
                                  $data = mysqli_fetch_assoc($data);
                                  $gelarBelakang = str_replace(",",", ", $data['gelar_belakang']);
                                  $gelarDpn = str_replace(",",", ", $data['gelar_depan']);

                                  $checkingClassDosen = sqlSelect($connectingToDb,"*","anggota_kelas","WHERE wali_kelas_id='".$data['id']."'");
                                  $checkingClassDosen = mysqli_num_rows($checkingClassDosen);
                                ?>
                                <tr>
                                  <td>
                                    <?= $gelarDpn.$data['nama'].", ".$gelarBelakang ?>
                                  </td>
                                  <td class="text-center"><?= $rows['email'] ?></td>
                                  <td class="text-center">
                                    <?= Date("d-m-Y", strtotime($data['join_at'])) ?>
                                  </td>
                                  <td class="text-center">
                                      <?php if($checkingClassDosen == 0) {?>
                                        <span class="rounded border p-1 border-success text-success">Dosen Biasa</span>
                                        <?php } else {?>
                                          <span class="rounded border p-1 border-danger text-danger">Wali Kelas</span>
                                      <?php } ?>
                                      <span></span>
                                  </td>
                                  <td>
                                  <div class="table-links">
                                      <div class="bullet"></div>
                                      <a href="javascript:void(0)" onclick="openModalEdit('<?= $rows['id'] ?>','<?= $data['id'] ?>','<?= $data['nama']?>','<?= $rows['email'] ?>','<?= $data['gelar_depan'] ?>','<?= $data['gelar_belakang'] ?>','<?=$data['join_at']?>')">Edit</a>
                                      <div class="bullet"></div>
                                      <a href="javascript:void(0);" onclick="deleteDosen('<?= $rows['rows']?>','<?= $data['nama'] ?>','formHapusDosen<?= $rows['id'] ?>')" class="text-danger">Hapus</a>
                                      <form action="../../controller/DeleteDosen.php" method="POST" id="formHapusDosen<?= $rows['id'] ?>">
                                        <input type="hidden" name="idUser" value="<?=$rows['id']?>">
                                        <input type="hidden" name="namaDosen" value="<?=$data['nama']?>">
                                    </form>
                                    </div>
                                  </td>
                                </tr>
                              <?php }?>
                            <?php } else {?>
                              <tr><td colspan="4" class="text-center fw-bold font-italic">Data kontak kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
                </div>
              </div>
            </section>
          </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Edit Data Dosen </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="../../controller/UpdateDosen.php" method="POST" id="formEdit">
                <input type="hidden" name="idUser">
                <input type="hidden" name="idDosen">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Dosen </label>
                    <input type="text" id="nama_dosen" name="nama_dosen"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama dosen..">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Gelar Depan </label>
                    <input type="text" id="gelar_dpn" name="gelar_dpn"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan gelar depan dosen">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Gelar Belakang </label>
                    <input type="text" id="gelar_belakang" name="gelar_belakang"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan gelar belakang dosen">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email NetId</label>
                    <input type="email" id="email" name="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Ganti Password</label>
                    <input type="password" id="password" name="password"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan password jika ingin ganti password....">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Join At</label>
                    <input type="date" id="join_at" name="join_at"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" id="submitEditBtn" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </div>
    </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      deleteDosen = (id, nama_kelas, idForm) => {
        let confirmation = confirm(`Apakah Anda yakin ingin menghapus dosen ${nama_kelas} ?`);
        if(confirmation) {
            let form = document.getElementById(idForm);
            form.submit();
        }
      }

      openModalEdit = (idUser, idDosen, nama_dosen, email, gelar_dpn, gelar_blng, join_at) => {
        $("#exampleModalLabel").text(`Edit Data Dosen ${nama_dosen}`)
        $("#exampleModal").modal("show");
        $("[name='nama_dosen']").val(nama_dosen);
        $("[name='idUser']").val(idUser);
        $("[name='idDosen']").val(idDosen);
        $("[name='email']").val(email);
        $("[name='gelar_belakang']").val(gelar_blng);
        $("[name='gelar_depan']").val(gelar_dpn);
        $("[name='join_at']").val(join_at);
      }

      $("#submitEditBtn").on("click", () => {
        let namaDosen = $("[name='nama_dosen']").val();
        let emailDosen = $("[name='email']").val();
        let gelarBelakang = $("[name='gelar_belakang']").val();
        let joinAt = $("[name='join_at']").val();
        if(namaDosen == '') {
            alert("Maaf nama dosen tidak boleh kosong");
        } else if(emailDosen == '') {
            alert("Maaf email dosen tidak boleh kosong");
        } else if(gelarBelakang == '') {
            alert("Maaf gelar belakang dosen tidak boleh kosong");
        } else if(joinAt == '') {
            alert("Maaf tanggal bergabung dosen tidak boleh kosong");
        } else {
            let form = document.getElementById('formEdit');
            form.submit();
        }
      });
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
