<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-mahasiswa.php");
    if($_SESSION['kelas'] == NULL) {
      $user = $_SESSION['users']['mahasiswa'];
      session_unset();
      session_destroy();
      session_start();
      $_SESSION['error_message'] = 'NetId '. $user['nama'] ." belum ada kelas";
      header('location: ../login.php');
  }

    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE kelas_id=".$_SESSION['kelas']['kelas_id']);
    $getDataTotalMatkul = mysqli_num_rows($querySelectMatkul);
    $querySelectWalas = sqlSelect($connectingToDb,"*","dosen","WHERE id=".$_SESSION['kelas']['wali_kelas_id']);
    $getDataTotalWalas = mysqli_fetch_assoc($querySelectWalas);
    $querySelectTotalMhsKelas = sqlSelect($connectingToDb,"*","anggota_kelas","WHERE kelas_id=".$_SESSION['kelas']['kelas_id']);
    $getSelectTotalMhsKelas =  mysqli_num_rows($querySelectTotalMhsKelas);
    $querySelectTugasAktif = sqlSelect($connectingToDb, "*","tugas t", "INNER JOIN matkul m ON m.id=t.matkul_id
                                                                        WHERE m.kelas_id='".$_SESSION['kelas']['kelas_id']."' AND t.status='0'");
    $getDataTugasAktif = mysqli_num_rows($querySelectTugasAktif);
    $querySelectTugasSelesai = sqlSelect($connectingToDb, "*","tugas t", "INNER JOIN matkul m ON m.id=t.matkul_id
                                                                        WHERE m.kelas_id='".$_SESSION['kelas']['kelas_id']."' AND t.status='1'");
    $getDataTugasSelesai = mysqli_num_rows($querySelectTugasSelesai);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Dashboard</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            </div>
          </div>
          <secion class="section">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Kelas</h4>
                  </div>
                  <div class="card-body">
                    <?= $getDataTotalMatkul ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                  <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Wali Kelas</h4>
                  </div>
                  <div class="font-weight-bold">
                  <?= $getDataTotalWalas['gelar_depan']." ".$getDataTotalWalas['nama'].", ".$getDataTotalWalas['gelar_belakang'] ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                  <!-- <i class="far fa-pencil"></i> -->
                  <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Nama Kelas</h4>
                  </div>
                  <div class="card-body">
                    <?= $_SESSION['kelas']['nama_kelas'] ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="fas fa-pen"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Tugas Selesai</h4>
                  </div>
                  <div class="card-body">
                    <?= $getDataTugasSelesai ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </secion>
          <div class="section-body">
            <h2 class="section-title">Halaman Dashboard</h2>
            <p class="section-lead">Halaman yang menyajikan statistik data keseluruhan</p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <?php if($_SESSION['kelas']) { ?>
                      <div class="card-header">
                        <h4>Data Mahasiswa (<?= $_SESSION['kelas']['nama_kelas'] ?>)</h4>
                      </div>
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <tbody><tr>
                            <th>#</th>
                            <th>NRP</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Jenis Kelamin</th>
                            <th class="text-center">Asal Kota</th>
                          </tr>
                          <?php $counter = 1; if($getSelectTotalMhsKelas > 0) { ?>
                            
                            <?php while($rows = mysqli_fetch_assoc($querySelectTotalMhsKelas)) {?>
                              <?php 
                                $data = sqlSelect($connectingToDb, "*", "mahasiswa","WHERE id='".$rows['mahasiswa_id']."'");
                                $data = mysqli_fetch_assoc($data);
                              ?>
                              <tr>
                                <td><?=$counter++ ?></td>
                                <td>
                                  <?= $data['nrp'] ?>
                                </td>
                                <td class="text-center"><?= $data['nama'] ?></td>
                                <td class="text-center">
                                  <?= $data['jenis_kelamin'] ?>
                                </td>
                                <td class="text-center">
                                  <?= $data['kota_asal'] ?>
                                </td>
                              </tr>
                            <?php }?>
                          <?php } else {?>
                            <tr><td colspan="6" class="text-center fw-bold font-italic">Data kosong</td></tr>
                          <?php } ?>
                        </tbody></table>
                      </div>
                    <?php } ?>
                    <div class="card-header">
                      <h4>Daftar Matakuliah <?= $_SESSION['users']['dosen']['nama'] ?></h4>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <tbody><tr>
                          <th>#</th>
                          <th class="text-center">Mata Kuliah</th>
                          <th class="text-center">Dosen Pengampu</th>
                        </tr>
                         <?php if($getDataTotalMatkul > 0) { ?>
                          <?php $counter = 1; while($rows = mysqli_fetch_assoc($querySelectMatkul)) {?>
                            <?php 
                              $data = sqlSelect($connectingToDb, "*", "dosen","WHERE id=".$rows['dosen_id']);
                              $data = mysqli_fetch_assoc($data);
                            ?>
                            <tr>
                              <td><?= $counter++ ?></td>
                              <td class="text-center">
                                <?= $rows['nama_matkul'] ?>
                              </td>
                              <td class="text-center"><?= $data['gelar'].$data['nama'].",".$data['gelar_belakang'] ?></td>
                            </tr>
                          <?php }?>
                        <?php } else {?>
                          <tr><td colspan="4" class="text-center fw-bold font-italic">Data kosong</td></tr>
                        <?php } ?> 
                      </tbody></table>
                    </div>
                    <div class="float-right">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
