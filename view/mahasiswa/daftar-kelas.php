<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-mahasiswa.php");

    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE kelas_id=".$_SESSION['kelas']['kelas_id']);
    $getDataTotalMatkul = mysqli_num_rows($querySelectMatkul);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    .jumbotron {
      padding: 2rem 2rem;
      background-color: #ffffff!important;
      box-shadow: 0 0 5px #cacaca!important;
    }

    .btn:not(.btn-social):not(.btn-social-icon):active, .btn:not(.btn-social):not(.btn-social-icon):focus, .btn:not(.btn-social):not(.btn-social-icon):hover {
        border-color: transparent !important;
        background-color: #009c1b!important;
        color: white!important;
    }
  </style>

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="../mahasiswa/dashboard.php">Dashboard</a></div>
              <div class="breadcrumb-item active">Kelas</div>
              <div class="breadcrumb-item active">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?> </h2>
            <p class="section-lead">Halaman yang menyajikan data matakuliah <?=$_SESSION['kelas']['nama_kelas']?></p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="container-fluid">
                      <div class="row">
                        <?php if($getDataTotalMatkul > 0) { ?>
                          <?php $counter = 1; while($rows = mysqli_fetch_assoc($querySelectMatkul)) {?>
                            <div class="col-md-4">
                            <?php 
                              $data = sqlSelect($connectingToDb, "*", "dosen","WHERE id=".$rows['dosen_id']);
                              $data = mysqli_fetch_assoc($data);
                            ?>
                              <div class="jumbotron">
                                <h5 class="">Kelas : </h5>
                                <h7 class=""><?=$rows['nama_matkul']?>.</h7>
                                <h5 class="">Dosen Pengampu :</h5>
                                <p><?=$data['gelar_depan']." ".$data['nama'].", ".$data['gelar_belakang']?></p>
                                <h5 class="">Jadwal :</h5>
                                <p><?=$rows['jam_kelas']?></p>
                                <a href="../mahasiswa/kelas.php?class_id=<?= base64_encode($rows['id']) ?>" class="btn btn-lg border-success text-success w-100" href="#" role="button">Masuk Kelas</a>
                              </div>
                            </div>
                          <?php }?>
                        <?php } else {?>
                          <div class="col-md-12">
                            <div class="jumbotron">
                                <p class="lead text-center">Belum Ada Kelas</p>
                              </div>
                          </div>
                        <?php } ?> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
