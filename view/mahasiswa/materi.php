<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-mahasiswa.php");

    $idMatkul = base64_decode($_GET['class_id']);
    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE id=".$idMatkul);
    $getDataTotalMatkul = mysqli_fetch_assoc($querySelectMatkul);
    $querySelectMatkulDosen = sqlSelect($connectingToDb,"*","dosen","WHERE id=".$getDataTotalMatkul['dosen_id']);
    $getDataTotalMatkulDosen = mysqli_fetch_assoc($querySelectMatkulDosen);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","WHERE id=".$getDataTotalMatkul['kelas_id']);
    $getDataTotalKelas = mysqli_fetch_assoc($querySelectKelas);
    $getDataAll = sqlSelect($connectingToDb,"m.nrp , m.nama, m.jenis_kelamin, m.kota_asal","anggota_kelas ak","INNER JOIN mahasiswa m ON m.id=ak.mahasiswa_id
                                            WHERE ak.kelas_id=".$getDataTotalMatkul['kelas_id']."");
    $queryDataMateri = sqlSelect($connectingToDb,"*","materi","WHERE matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataMateri = mysqli_num_rows($queryDataMateri);
    $queryDataTugas = sqlSelect($connectingToDb,"*","tugas","WHERE status='0' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugas = mysqli_num_rows($queryDataTugas);
    $queryDataTugasSelesai = sqlSelect($connectingToDb,"*","tugas","WHERE status='1' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugasSelesai = mysqli_num_rows($queryDataTugasSelesai);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    .jumbotron {
      padding: 2rem 2rem;
      background-color: #ffffff!important;
      box-shadow: 0 0 5px #cacaca!important;
    }

    .btn:not(.btn-social):not(.btn-social-icon):active, .btn:not(.btn-social):not(.btn-social-icon):focus, .btn:not(.btn-social):not(.btn-social-icon):hover {
        border-color: transparent !important;
        background-color: #009c1b!important;
        color: white!important;
    }

    .bdr {
        border: 1px solid #029c1c!important;
        box-shadow: 0 0 1px 1px #029c1c!important;
    }
  </style>

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?= $getDataTotalMatkul['nama_matkul']?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="../mahasiswa/dashboard.php">Dashboard</a></div>
              <div class="breadcrumb-item active"><a href="../mahasiswa/daftar-kelas.php">Kelas</a></div>
              <div class="breadcrumb-item active"> <a href="../mahasiswa/daftar-kelas.php">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></a></div>
              <div class="breadcrumb-item active">Kelas <?= $getDataTotalMatkul['nama_matkul']?></div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Kelas <?= $getDataTotalMatkul['nama_matkul']?> </h2>
            <p class="section-lead">Halaman yang menyajikan data mata kuliah <?= $getDataTotalMatkul['nama_matkul']." kelas ".$getDataTotalKelas['nama_kelas']?></p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                  <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-6">
                                <h5 class="">Mata Kuliah :</h5>
                                <p><?=$getDataTotalMatkul['nama_matkul']?></p>
                                <h5 class="">Dosen Pengampu : </h5>
                                <p class=""><?=$getDataTotalMatkulDosen['gelar_depam']." ".$getDataTotalMatkulDosen['nama'].", ".$getDataTotalMatkulDosen['gelar_belakang']?>.</p>
                                <h5 class="">Jadwal :</h5>
                                <p><?=$getDataTotalMatkul['jam_kelas']?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <a href="../mahasiswa/kelas.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Mahasiswa</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= mysqli_num_rows($getDataAll) ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#">
                                                <div class="card card-statistic-1 bdr">
                                                    <div class="card-icon bg-danger">
                                                    <i class="fas fa-book"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Materi</h4>
                                                    </div>
                                                    <div class="card-body">
                                                    <?= $getTotalDataMateri ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                          <a href="../mahasiswa/tugas-aktif.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                            <div class="card card-statistic-1">
                                                <div class="card-icon bg-warning">
                                                <!-- <i class="far fa-pencil"></i> -->
                                                <i class="fas fa-pen"></i>
                                                </div>
                                                <div class="card-wrap">
                                                <div class="card-header">
                                                    <h4>Tugas Aktif</h4>
                                                </div>
                                                <div class="card-body">
                                                    <?= $getTotalDataTugas ?>
                                                </div>
                                                </div>
                                            </div>
                                          </a>
                                        </div>
                                        <div class="col-md-6">
                                          <a href="../mahasiswa/tugas-selesai.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                            <div class="card card-statistic-1">
                                                <div class="card-icon bg-success">
                                                <i class="fas fa-pen"></i>
                                                </div>
                                                <div class="card-wrap">
                                                <div class="card-header">
                                                    <h4>Tugas Selesai</h4>
                                                </div>
                                                <div class="card-body">
                                                    <?= $getTotalDataTugasSelesai ?>
                                                </div>
                                                </div>
                                            </div>
                                          </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($getDataTotalMatkul['link_meet']) { ?>
                                <div class="col">
                                    <a href="<?= $getDataTotalMatkul['link_meet'] ?>" target="_blank" class="fw-bold btn border-success w-100 text-success">Masuk Kelas Online</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                     <div class="card-header"> 
                        <h4>Materi <?= $getDataTotalMatkul['nama_matkul']." ".$getDataTotalKelas['nama_kelas']?></h4>
                      </div>
                      <div class="container-fluid">
                        <div class="row">
                          <?php if($getTotalDataMateri > 0) { ?>
                            <?php $counter = 1; while($rows = mysqli_fetch_assoc($queryDataMateri)) {?>
                              <div class="col-md-6">
                                <div class="jumbotron">
                                  <h7 class="font-weight-bold text-success">Materi </h7>
                                  <p class=""><?=$rows['nama']?></p>
                                  <h7 class="font-weight-bold text-info">Waktu Upload</h7>
                                  <p><?=date('d M Y H:i:s', strtotime($rows['created_at']))?></p>
                                  <h7 class="font-weight-bold text-warning">Size File</h7>
                                  <p><?=convertBytes($rows['size'])?></p>
                                  <div id="accordion">
                                        <div class="card">
                                          <div class="" id="headingOne">
                                            <h5 class="mb-0">
                                              <button class="btn border-success text-success" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Deskripsi Materi
                                              </button>
                                            </h5>
                                          </div>

                                          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                              <?=$rows['deskripsi'] == "" ? "</span class='font-italic'>Tidak ada deskripsi tugas</span>" : $rows['deskripsi']?>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  <div class="d-flex justify-content-center">
                                    <a href="../../controller/DownloadMateriController.php?code_file=<?=base64_encode($rows['id'])?>" class="mx-2 w-100 btn btn-info w-100" href="#" role="button"><i class="fa fa-download mx-2"></i>Download File</a>
                                  </div>
                                  
                                </div>
                              </div>
                            <?php }?>
                          <?php } else {?>
                            <div class="col-md-12">
                              <div class="jumbotron">
                                  <p class="lead text-center m-0">Belum Ada Materi</p>
                                </div>
                            </div>
                          <?php } ?> 
                        </div>
                    </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
    
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>

      openModal = () => {
          $("#exampleModal").modal("show");
      }

      openModal2 = () => {
          $("#exampleModal2").modal("show");
      }

      $("#submitEditBtn").on("click",() => {
        let form = document.getElementById('formEdit');
        let linkMeet = $("#link_meet").val();
        if(!linkMeet) {
            alert("Maaf... silahkan isi dahulu link meetnya");
        } else {
            form.submit();
        }
      });

      $("#submitMateri").on("click", () => {
        let form = document.getElementById('formMateri');
        let materi = $("#nama_materi").val();
        let file = $("#inputFile");
        file = file[0].files[0];
        if(!materi) {
            alert("Maaf... silahkan isi nama materinya");
        } else if(!file) {
            alert("Maaf... silahkan lampirkan file materinya");
        } else {
            form.submit();
        }
      })

      deleteMateri = (nama_file, namaForm) => {
        let confirmation = confirm(`Apakah Anda ingin menghapus materi ${nama_file} ?`);
        if(confirmation) {
          let form = document.getElementById(namaForm);
          form.submit();
        }
      }

      fileName = (self) => {
        let input = $(self);
        let fileName = input[0].files[0].name;
        let imgEL = ``
        $("#labelCLick").text(imgEL+fileName);
      }
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
