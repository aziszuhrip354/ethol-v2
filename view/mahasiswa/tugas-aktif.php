<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-mahasiswa.php");

    $idMatkul = base64_decode($_GET['class_id']);
    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE id=".$idMatkul);
    $getDataTotalMatkul = mysqli_fetch_assoc($querySelectMatkul);
    $querySelectMatkulDosen = sqlSelect($connectingToDb,"*","dosen","WHERE id=".$getDataTotalMatkul['dosen_id']);
    $getDataTotalMatkulDosen = mysqli_fetch_assoc($querySelectMatkulDosen);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","WHERE id=".$getDataTotalMatkul['kelas_id']);
    $queryDataTugas = sqlSelect($connectingToDb,"*","tugas","WHERE status='0' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugas = mysqli_num_rows($queryDataTugas);
    $queryDataMateri = sqlSelect($connectingToDb,"*","materi","WHERE matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataMateri = mysqli_num_rows($queryDataMateri);
    $queryDataTugasSelesai = sqlSelect($connectingToDb,"*","tugas","WHERE status='1' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugasSelesai = mysqli_num_rows($queryDataTugasSelesai);
    $getDataTotalKelas = mysqli_fetch_assoc($querySelectKelas);
    $getDataAll = sqlSelect($connectingToDb,"m.nrp , m.nama, m.jenis_kelamin, m.kota_asal","anggota_kelas ak","INNER JOIN mahasiswa m ON m.id=ak.mahasiswa_id
                                            WHERE ak.kelas_id=".$getDataTotalMatkul['kelas_id']."");
    date_default_timezone_set("Asia/Jakarta");

    $fileTugas = $_FILES['file'];
    if($fileTugas) {
      $mahasiswaId = $_SESSION['kelas']['mahasiswa_id'];
      $idTugas = $_POST['idTugas'];
      $desc = $_POST['deskripsi'];
      $tmpName = $fileTugas['tmp_name'];
      $typeFile = $fileTugas['type'];
      $fileNameOri = basename($fileTugas['name']);
      $sizeFile = $fileTugas['size'];
      $pathFile = "../../files_users/tugas/". $fileNameOri;
      $movingFileToPath = move_uploaded_file($tmpName, $pathFile);
      if($movingFileToPath) {
        $sqlInsertDataFile = "INSERT INTO submit_tugas (tugas_id, mahasiswa_id, message, name, path, size, type) 
                              VALUES ('$idTugas', '$mahasiswaId', '$desc','$fileNameOri','$pathFile','$sizeFile','$typeFile')";
        if(mysqli_query($connectingToDb, $sqlInsertDataFile)) {
          $_SESSION['success_message'] = 'Berhasil mengupload tugas file';
          header("Refresh:0");
        } else {
          $_SESSION['error_message'] = "Database error: ".mysqli_error($connectingToDb);
         }
      } else {
        $_SESSION['error_message'] = 'Gagal mengupload file tugas';
      }
    }

    $fileTugas2 = $_FILES['fileEdit'];
    if($fileTugas2) {
      $mahasiswaId = $_SESSION['kelas']['mahasiswa_id'];
      $idTugas = $_POST['idTugasEdit'];
      $desc = $_POST['deskripsi'];
      $tmpName = $fileTugas2['tmp_name'];
      $typeFile = $fileTugas2['type'];
      $fileNameOri = basename($fileTugas2['name']);
      $sizeFile = $fileTugas2['size'];
      $pathFile = "../../files_users/tugas/". $fileNameOri;
      $movingFileToPath = move_uploaded_file($tmpName, $pathFile);
      $pathFile = "../files_users/tugas/". $fileNameOri;
      $submitedAt = date("Y-m-d H:i:s", time());
      $pathFileLast = base64_decode($_POST['pathFile']);
      if($movingFileToPath) {
        $sqlInsertDataFile = "UPDATE submit_tugas SET submited_at='$submitedAt', message='$desc', name='$fileNameOri', path='$pathFile', size='$sizeFile', type='$typeFile'
                              WHERE id='$idTugas'";
        if(mysqli_query($connectingToDb, $sqlInsertDataFile)) {
          if(file_exists("../".$pathFileLast)) {
            unlink("../".$pathFileLast);
          }
          $_SESSION['success_message'] = 'Berhasil mengupload tugas file';
          header("Refresh:0");
        } else {
          $_SESSION['error_message'] = "Database error: ".mysqli_error($connectingToDb);
         }
      } else {
        $_SESSION['error_message'] = 'Gagal mengupload file tugas';
      }
    }


    if(!$getDataTotalMatkul) {
        header('Location: ../mahasiswa/daftar-kelas.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    .jumbotron {
      padding: 2rem 2rem;
      background-color: #ffffff!important;
      box-shadow: 0 0 5px #cacaca!important;
    }

    .btn:not(.btn-social):not(.btn-social-icon):active, .btn:not(.btn-social):not(.btn-social-icon):focus, .btn:not(.btn-social):not(.btn-social-icon):hover {
        border-color: transparent !important;
        background-color: #009c1b!important;
        color: white!important;
    }

    .bdr {
        border: 1px solid #029c1c!important;
        box-shadow: 0 0 1px 1px #029c1c!important;
    }
  </style>

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?= $getDataTotalMatkul['nama_matkul']?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="../mahasiswa/dashboard.php">Dashboard</a></div>
              <div class="breadcrumb-item active"><a href="../mahasiswa/daftar-kelas.php">Kelas</a></div>
              <div class="breadcrumb-item active"> <a href="../mahasiswa/daftar-kelas.php">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></a></div>
              <div class="breadcrumb-item active">Kelas <?= $getDataTotalMatkul['nama_matkul']?></div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Kelas <?= $getDataTotalMatkul['nama_matkul']?> </h2>
            <p class="section-lead">Halaman yang menyajikan data mata kuliah <?= $getDataTotalMatkul['nama_matkul']." kelas ".$getDataTotalKelas['nama_kelas']?></p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                  <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-6">
                                <h5 class="">Mata Kuliah :</h5>
                                <p><?=$getDataTotalMatkul['nama_matkul']?></p>
                                <h5 class="">Dosen Pengampu : </h5>
                                <p class=""><?=$getDataTotalMatkulDosen['gelar_depam']." ".$getDataTotalMatkulDosen['nama'].", ".$getDataTotalMatkulDosen['gelar_belakang']?>.</p>
                                <h5 class="">Jadwal :</h5>
                                <p><?=$getDataTotalMatkul['jam_kelas']?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <a href="../mahasiswa/kelas.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1 ">
                                                    <div class="card-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Mahasiswa</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= mysqli_num_rows($getDataAll) ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../mahasiswa/materi.php?class_id=<?= base64_encode($getDataTotalMatkul['id'])?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-danger">
                                                    <i class="fas fa-book"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Materi</h4>
                                                    </div>
                                                    <div class="card-body">
                                                    <?= $getTotalDataMateri ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#">
                                                <div class="card card-statistic-1 bdr">
                                                    <div class="card-icon bg-warning">
                                                    <!-- <i class="far fa-pencil"></i> -->
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Aktif</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= $getTotalDataTugas ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../mahasiswa/tugas-selesai.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-success">
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Selesai</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?=$getTotalDataTugasSelesai ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <?php if($getDataTotalMatkul['link_meet']) { ?>
                                <div class="col">
                                    <a href="<?= $getDataTotalMatkul['link_meet'] ?>" target="_blank" class="fw-bold btn border-success w-100 text-success">Masuk Kelas Online</a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                <div class="card-header">
                        <h4>Tugas Mata Kuliah <?= $getDataTotalMatkul['nama_matkul']." ".$getDataTotalKelas['nama_kelas']?></h4>
                      </div>
                      <div class="container-fluid">
                        <div class="row">
                          <?php if($getTotalDataTugas > 0) { ?>
                            
                            <?php $counter = 1; while($rows = mysqli_fetch_assoc($queryDataTugas)) {?>
                              <?php 
                                      $deadline = explode(" ", $rows['deadline']);
                                      $hari = $deadline[0];
                                      $waktu = str_replace(":00","",$deadline[1]);
                                      $checklistTugas = sqlSelect($connectingToDb, "*","submit_tugas"," WHERE tugas_id='".$rows['id']."' AND mahasiswa_id='".$_SESSION['kelas']['mahasiswa_id']."'");
                                      $checklistTugas = mysqli_fetch_assoc($checklistTugas);
                                      // var_dump($checklistTugas);
                              ?>
                              <div class="col-md-6">
                                <div class="jumbotron">
                                  <h7 class="font-weight-bold text-success">Tugas </h7>
                                  <p class=""><?=$rows['nama_tugas']?></p>
                                  <h7 class="font-weight-bold text-gray">Status </h7>
                                  <?php if($checklistTugas) { ?>
                                    <br>
                                    <span class="text-white p-1 rounded my-2 bg-success">Sudah Mengumpulkan</span>
                                    <br>
                                    <br>
                                  <?php } else {?>
                                    <br>
                                    <span class="text-white p-1 rounded bg-danger">Belum Mengumpulkan</span>
                                    <br>
                                    <br>
                                  <?php }?>
                                  <h7 class="font-weight-bold text-danger">Deadline</h7>
                                  <p><?=date('d M Y H:i:s', strtotime($rows['deadline']))?></p>
                                  <div id="accordion">
                                    <div class="card">
                                      <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                          <button class="btn border border-success text-success" data-toggle="collapse" data-target="#collapseOne<?=$rows['id']?>" aria-expanded="true" aria-controls="collapseOne<?=$rows['id']?>">
                                            Detail Tugas
                                          </button>
                                        </h5>
                                      </div>

                                      <div id="collapseOne<?=$rows['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                          <h6>Deskripsi Tugas</h6>
                                          <div class="card p-2 border border-gray">
                                            <?= $rows['deskripsi'] == "" ? "<span class='font-italic'> Tidak ada deskripsi</span>" : $rows['deskripsi']?>
                                          </div>
                                          <?php if($checklistTugas) { ?>
                                            
                                          <?php } ?>
                                        </div>
                                      </div>
                                      <?php if($checklistTugas) { ?>
                                        <div class="card-header" id="headingTwo">
                                          <h5 class="mb-0">
                                            <button class="btn border border-success text-success" data-toggle="collapse" data-target="#collapseOne<?=$checklistTugas['id']?>" aria-expanded="true" aria-controls="collapseOne<?=$checklistTugas['id']?>">
                                              Detail Jawaban
                                            </button>
                                          </h5>
                                        </div>
                                        <div id="collapseOne<?=$checklistTugas['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                          <div class="card-body">
                                              <h6>Pesanmu Untuk Dosen</h6>
                                              <div class="card card p-2 border border-gray">
                                                <?= $checklistTugas['message']?>
                                              </div>
                                              <h6>Waktu Pengumpulan</h6>
                                              <div class="card card p-2 border border-gray"><?=date("d M Y H:i:s", strtotime($checklistTugas['submited_at']))?></div>
                                              <div class="form-group">
                                                  <label for="" class="">File Tugas</label>
                                                  <div class="input-group">
                                                        <label for="inputFile" class="form-control font-italic" id="labelCLick"> <?=$checklistTugas['name']?></label>
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text text-success">
                                                              <a href="../../controller/DownloadTugasControllerr.php?code_file=<?= base64_encode($checklistTugas['id']) ?>">
                                                                <i class="fa fa-download"></i>
                                                              </a>
                                                            </div>
                                                        </div>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <div class="d-flex justify-content-center">
                                    <?php if(date("Y-m-d H:i:s") < date("Y-m-d H:i:s", strtotime($rows['deadline']))) {?>
                                      <?php if($checklistTugas) {?>
                                        <a href="javascript:void(0)" onclick="openModal2('<?=$checklistTugas['id']?>','<?=$rows['nama_tugas']?>','<?= base64_encode($checklistTugas['message'])?>','<?=base64_encode($checklistTugas['path'])?>')" class="mx-2 w-50 btn border-success text-success w-100" href="#" role="button">Kirim Ulang tugas</i></a>
                                      <?php } else {?>
                                        <a href="javascript:void(0)" onclick="openModal('<?=$rows['id']?>','<?=$rows['nama_tugas']?>')" class="mx-2 w-50 btn border-info text-info w-100" href="#" role="button">Kumpulkan tugas</i></a>
                                      <?php }?>
                                    <?php } else { ?>
                                        <a href="javascript:void(0)" class="btn border-danger text-danger font-italic w-100">Deadline tugas berakhir</a>
                                    <?php } ?>
                                    </div>
                                  </div>
                              </div>
                            <?php }?>
                          <?php } else {?>
                            <div class="col-md-12">
                              <div class="jumbotron">
                                  <p class="lead text-center m-0">Belum Ada Tugas Aktif</p>
                                </div>
                            </div>
                          <?php } ?> 
                        </div>
                    </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel"><h7 class="font-weight-bold" id="tugas_desc"></h7>
 </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" enctype="multipart/form-data" class="mt-2" id="formTugas">
                <input type="hidden" name="idTugas" value="">
                <div class="form-group">
                  <label>Pesan Untuk Dosen</label>
                  <textarea id="default-editor" name="deskripsi"></textarea>
                </div>
                <div class="form-group">
                      <label for="inputFile" class="">File Tugas</label>
                      <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                  <i class="fa fa-file"></i>
                                </div>
                            </div>
                            <label for="inputFile" class="form-control font-italic" id="labelCLick"> klik dan lampirkan file</label>
                            <input type="file" onchange="fileName(this)" name="file" class="form-control-file d-none" id="inputFile" required>
                      </div>
                  </div>
                
            </form>
            <button type="button" id="submitTugas" class="btn btn-primary w-100">Simpan</button>
        </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel"><h7 class="font-weight-bold" id="tugas_desc_edit"></h7>
 </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" enctype="multipart/form-data" class="mt-2" id="formTugasEdit">
                <input type="hidden" name="idTugasEdit" value="">
                <input type="hidden" name="pathFile" value="">
                <div class="form-group">
                  <label>Pesan Untuk Dosen</label>
                  <textarea id="default-editor2" name="deskripsi"></textarea>
                </div>
                <div class="form-group">
                      <label for="inputFile" class="">File Tugas</label>
                      <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                  <i class="fa fa-file"></i>
                                </div>
                            </div>
                            <label for="fileEdit" class="form-control font-italic" id="labelCLick2"> klik dan lampirkan file</label>
                            <input type="file" onchange="fileName2(this)" name="fileEdit" class="form-control-file d-none" id="fileEdit" required>
                      </div>
                  </div>
                
            </form>
            <button type="button" id="submitEditTugas" class="btn btn-primary w-100">Simpan</button>
        </div>
        </div>
    </div>
    </div>


  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@1/dist/tinymce-jquery.min.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      tinymce.init({
        selector: 'textarea#default-editor'
      });

      tinymce.init({
        selector: 'textarea#default-editor2'
      });

      openModal = (idTugas, namaTugas) => {
          $("#exampleModal").modal("show");
          $("[name='idTugas']").val(idTugas);
          $("#tugas_desc").text(namaTugas);
      }

      openModal2 = (idTugas, nama_tugas, message, pathFile) => {
        $("#exampleModal2").modal("show");
        $("[name='idTugasEdit']").val(idTugas);
        $("[name='pathFile']").val(pathFile);
        $("#tugas_desc_edit").text(`Edit Tugas ${nama_tugas}`)
        tinymce.get("default-editor2").setContent(atob(message));
      }

      $("#submitTugas").on("click", () => {
        let form = document.getElementById('formTugas');
        let file = $("#inputFile");
        file = file[0].files[0];
        if(!file) {
            alert("Maaf... silahkan lampirkan file pengerjaan tugasnya");
        } else {
            form.submit();
        }
      })

      $("#submitEditTugas").on("click", () => {
        let form = document.getElementById('formTugasEdit');
        let file = $("#fileEdit");
        file = file[0].files[0];
        if(!file) {
            alert("Maaf... silahkan isi nama materinya");
        } else {
            form.submit();
        }
      })

      fileName = (self) => {
        let input = $(self);
        let fileName = input[0].files[0].name;
        let imgEL = ``
        $("#labelCLick").text(imgEL+fileName);
      }

      fileName2 = (self) => {
        let input = $(self);
        let fileName = input[0].files[0].name;
        let imgEL = ``
        $("#labelCLick2").text(imgEL+fileName);
      }


      $("#submitEditBtn").on("click",() => {
        let form = document.getElementById('formEdit');
        let linkMeet = $("#link_meet").val();
        if(!linkMeet) {
            alert("Maaf... silahkan isi dahulu link meetnya");
        } else {
            form.submit();
        }
      });

      closeTugas = (nama_tugas, idForm) => {
        let confirmation = confirm(`Apakah anda ingin menutup tugas ${nama_tugas} ? `);
        if(confirmation) {
          let form = document.getElementById(idForm);
          form.submit();
        }
      }
      
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
