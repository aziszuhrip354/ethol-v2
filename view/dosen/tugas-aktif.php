<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-dosen.php");

    $idMatkul = base64_decode($_GET['class_id']);
    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE id=".$idMatkul);
    $getDataTotalMatkul = mysqli_fetch_assoc($querySelectMatkul);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","WHERE id=".$getDataTotalMatkul['kelas_id']);
    $queryDataTugas = sqlSelect($connectingToDb,"*","tugas","WHERE status='0' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugas = mysqli_num_rows($queryDataTugas);
    $queryDataMateri = sqlSelect($connectingToDb,"*","materi","WHERE matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataMateri = mysqli_num_rows($queryDataMateri);
    $queryDataTugasSelesai = sqlSelect($connectingToDb,"*","tugas","WHERE status='1' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugasSelesai = mysqli_num_rows($queryDataTugasSelesai);
    $getDataTotalKelas = mysqli_fetch_assoc($querySelectKelas);
    $getDataAll = sqlSelect($connectingToDb,"m.nrp , m.nama, m.jenis_kelamin, m.kota_asal","anggota_kelas ak","INNER JOIN mahasiswa m ON m.id=ak.mahasiswa_id
                                            WHERE ak.kelas_id=".$getDataTotalMatkul['kelas_id']."");
    
    // $getDataAll =  mysqli_fetch_assoc($getDataAll);
    // dd($getDataTotalMatkul['kelas_id']);
    if(!$getDataTotalMatkul) {
        header('Location: ../dosen/daftar-kelas.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    .jumbotron {
      padding: 2rem 2rem;
      background-color: #ffffff!important;
      box-shadow: 0 0 5px #cacaca!important;
    }

    .btn:not(.btn-social):not(.btn-social-icon):active, .btn:not(.btn-social):not(.btn-social-icon):focus, .btn:not(.btn-social):not(.btn-social-icon):hover {
        border-color: transparent !important;
        background-color: #009c1b!important;
        color: white!important;
    }

    .bdr {
        border: 1px solid #029c1c!important;
        box-shadow: 0 0 1px 1px #029c1c!important;
    }
  </style>

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?= $getDataTotalMatkul['nama_matkul']?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="../dosen/dashboard.php">Dashboard</a></div>
              <div class="breadcrumb-item active"><a href="../dosen/daftar-kelas.php">Kelas</a></div>
              <div class="breadcrumb-item active"> <a href="../dosen/daftar-kelas.php">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></a></div>
              <div class="breadcrumb-item active">Kelas <?= $getDataTotalMatkul['nama_matkul']?></div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Kelas <?= $getDataTotalMatkul['nama_matkul']?> </h2>
            <p class="section-lead">Halaman yang menyajikan data mata kuliah <?= $getDataTotalMatkul['nama_matkul']." kelas ".$getDataTotalKelas['nama_kelas']?></p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                  <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="">Kelas : </h5>
                                <p class="lead"><?=$getDataTotalKelas['nama_kelas']?>.</p>
                                <h5 class="">Mata Kuliah :</h5>
                                <p><?=$getDataTotalMatkul['nama_matkul']?></p>
                                <h5 class="">Jadwal :</h5>
                                <p><?=$getDataTotalMatkul['jam_kelas']?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <a href="../dosen/kelas.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1 ">
                                                    <div class="card-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Mahasiswa</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= mysqli_num_rows($getDataAll) ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../dosen/materi.php?class_id=<?= base64_encode($getDataTotalMatkul['id'])?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-danger">
                                                    <i class="fas fa-book"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Materi</h4>
                                                    </div>
                                                    <div class="card-body">
                                                    <?= $getTotalDataMateri ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#">
                                                <div class="card card-statistic-1 bdr">
                                                    <div class="card-icon bg-warning">
                                                    <!-- <i class="far fa-pencil"></i> -->
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Aktif</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= $getTotalDataTugas ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../dosen/tugas-selesai.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-success">
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Selesai</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?=$getTotalDataTugasSelesai ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <?php  if($getDataTotalMatkul['link_meet']) { ?>
                                <div class="col">
                                    <a href="<?= $getDataTotalMatkul['link_meet'] ?>" target="_blank" class="fw-bold btn border-success w-100 text-success">Masuk Kelas Online</a>
                                </div>
                            <?php } ?>
                                <div class="col">
                                    <a href="javascript:void(0)" onclick="openModal();" class="fw-bold btn border-warning w-100 text-warning"><?= $getDataTotalMatkul['link_meet'] ? "Ganti Link Meet Kelas Online" : "Setting Link Meet Online" ?></a>
                                </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                <div class="card-header">
                        <h4>Tugas Mata Kuliah <?= $getDataTotalMatkul['nama_matkul']." ".$getDataTotalKelas['nama_kelas']?></h4>
                      </div>
                      <div class="container">
                        <div class="row">
                          <div class="col-md-2">
                            <button onclick="openModal2();" class="btn btn-success my-3">Buat Tugas</button>

                          </div>
                        </div>
                      </div>
                      <div class="container-fluid">
                        <div class="row">
                          <?php if($getTotalDataTugas > 0) { ?>
                            
                            <?php $counter = 1; while($rows = mysqli_fetch_assoc($queryDataTugas)) {?>
                              <div class="col-md-6">
                                <div class="jumbotron">
                                  <h7 class="font-weight-bold text-success">Tugas </h7>
                                  <p class=""><?=$rows['nama_tugas']?></p>
                                  <h7 class="font-weight-bold text-danger">Deadline</h7>
                                  <p><?=date('d M Y H:i:s', strtotime($rows['deadline']))?></p>
                                  <div id="accordion">
                                    <div class="card">
                                      <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                          <button class="btn border border-success text-success" data-toggle="collapse" data-target="#collapseOne<?=$rows['id']?>" aria-expanded="true" aria-controls="collapseOne<?=$rows['id']?>">
                                            Deskripsi Tugas
                                          </button>
                                        </h5>
                                      </div>

                                      <div id="collapseOne<?=$rows['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                          <?= $rows['deskripsi'] == "" ? "<span class='font-italic'> Tidak ada deskripsi</span>" : $rows['deskripsi']?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="d-flex justify-content-center">
                                    <?php 
                                      $deadline = explode(" ", $rows['deadline']);
                                      $hari = $deadline[0];
                                      $waktu = str_replace(":00","",$deadline[1]);
                                    ?>
                                    <a href="javascript:void(0)" onclick="openModal3('<?=$rows['id']?>','<?=$rows['nama_tugas']?>','<?=$hari?>','<?=$waktu?>','<?=$rows['deskripsi']?>')" class="mx-2 w-50 btn btn-info" href="#" role="button">Edit</i></a>
                                    <a href="javascript:void(0)" onclick="closeTugas('<?=$rows['nama_tugas']?>','tutupTugas<?=$rows['id']?>')" class="mx-2 w-50 btn btn-warning" role="button">Tutup Tugas</i></a>
                                    <form action="../../controller/TutupTugasController.php" method="POST" id="tutupTugas<?=$rows['id']?>">
                                        <input type="hidden" name="idTugas" value="<?=$rows['id']?>">
                                        <input type="hidden" name="namaTugas" value="<?=$rows['nama_tugas']?>">
                                    </form>
                                    <a href="javascript:void(0);" onclick="deleteTugas('<?=$rows['nama_tugas']?>','formDeleteTugas<?=$rows['id']?>')" class="mx-2 w-50 btn btn-danger" href="#" role="button">Hapus Tugas</a>
                                    <form action="../../controller/deleteTugasdeleteTugas.php" method="POST" id="formDeleteTugas<?=$rows['id']?>" class="d-none">
                                      <input type="hidden" name="idTugas" value="<?=$rows['id']?>">
                                      <input type="hidden" name="namaTugas" value="<?=$rows['nama_tugas']?>">
                                    </form>
                                  </div>
                                  
                                </div>
                              </div>
                            <?php }?>
                          <?php } else {?>
                            <div class="col-md-12">
                              <div class="jumbotron">
                                  <p class="lead text-center m-0">Belum Ada Tugas Aktif</p>
                                </div>
                            </div>
                          <?php } ?> 
                        </div>
                    </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Edit Link Meet </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="../../controller/UpdateLinkMeet.php" method="POST" id="formEdit">
                <input type="hidden" name="idMatkul" value="<?= $getDataTotalMatkul['id'] ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Link Meet </label>
                    <input type="text" id="link_meet" name="link_meet" value="<?= $getDataTotalMatkul['link_meet'] ?>"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan link meeting kelas online...">
                </div>
            </form>
                <button type="button" id="submitEditBtn" class="btn btn-primary w-100">Simpan</button>
        </div>
        </div>
    </div>
    </div>
  
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLabel">Edit Tugas </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="../../controller/EditTugasController.php" method="POST" id="formTugasEdit">
                  <input type="hidden" name="idTugas" value="">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Tugas </label>
                      <input type="text" id="edit_nama_tugas" name="edit_nama_tugas"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama tugas">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Deskripsi Tugas </label>
                      <textarea id="default-editor2" name="edit_deskripsi"></textarea>
                  </div>
                  <div class="form-group">
                      <label for="">Deadline</label>
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="">Hari</label>
                              <input type="date" name="edit_deadline_hari" class="form-control">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="">Jam</label>
                              <input type="time" name="edit_deadline_waktu" class="form-control">
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </form>
                  <button type="button" id="editSubmitFormTugas" class="btn btn-primary w-100">Simpan</button>
          </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLabel">Tambah Tugas </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="../../controller/BuatTugasController.php" method="POST" id="formTugas">
                  <input type="hidden" name="idMatkul" value="<?= $getDataTotalMatkul['id'] ?>">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Tugas </label>
                      <input type="text" id="nama_tugas" name="nama_tugas"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama tugas">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Deskripsi Tugas </label>
                      <textarea id="default-editor" name="deskripsi"></textarea>
                  </div>
                  <div class="form-group">
                      <label for="">Deadline</label>
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="">Hari</label>
                              <input type="date" name="deadline_hari" class="form-control">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="">Jam</label>
                              <input type="time" name="deadline_waktu" class="form-control">
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </form>
                  <button type="button" id="submitFormTugas" class="btn btn-primary w-100">Simpan</button>
          </div>
          </div>
      </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@1/dist/tinymce-jquery.min.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      tinymce.init({
        selector: 'textarea#default-editor'
      });

      tinymce.init({
        selector: 'textarea#default-editor2'
      });

      openModal = () => {
          $("#exampleModal").modal("show");
      }

      openModal2 = () => {
          $("#exampleModal2").modal("show");
      }

      openModal3 = (idTugas, nama_tugas, hari, waktu, desc) => {
        $("#exampleModal3").modal("show");
        $("[name='edit_nama_tugas']").val(nama_tugas);
        $("[name='edit_deadline_hari']").val(hari);
        $("[name='edit_deadline_waktu']").val(waktu);
        $("[name='edit_deskripsi']").html(desc);
        $("[name='idTugas']").val(idTugas);
        tinymce.get("default-editor2").setContent(desc);
      }

      $("#submitEditBtn").on("click",() => {
        let form = document.getElementById('formEdit');
        let linkMeet = $("#link_meet").val();
        if(!linkMeet) {
            alert("Maaf... silahkan isi dahulu link meetnya");
        } else {
            form.submit();
        }
      });

      closeTugas = (nama_tugas, idForm) => {
        let confirmation = confirm(`Apakah anda ingin menutup tugas ${nama_tugas} ? `);
        if(confirmation) {
          let form = document.getElementById(idForm);
          form.submit();
        }
      }

      $("#submitFormTugas").on("click", () => {
        let form = document.getElementById("formTugas");
        let nama_tugas = $("[name='nama_tugas']").val();
        let hari = $("[name='deadline_hari']").val();
        let waktu = $("[name='deadline_waktu']").val();
        if(!nama_tugas) {
          alert("Silahkan masukan dahulu nama tugasnya");
        } else if(!hari) {
          alert("Silahkan deadline hari tugasnya");
        } else if(!waktu) {
          alert("Silahkan deadline jam tugasnya");
        } else {
          form.submit();
        }
      });

      $("#editSubmitFormTugas").on("click", () => {
        let form = document.getElementById("formTugasEdit");
        let nama_tugas = $("[name='edit_nama_tugas']").val();
        let hari = $("[name='edit_deadline_hari']").val();
        let waktu = $("[name='edit_deadline_waktu']").val();
        if(!nama_tugas) {
          alert("Silahkan masukan dahulu nama tugasnya");
        } else if(!hari) {
          alert("Silahkan deadline hari tugasnya");
        } else if(!waktu) {
          alert("Silahkan deadline jam tugasnya");
        } else {
          form.submit();
        }
      });

      deleteTugas = (nama_tugas, idForm) => {
        let confirmation = confirm(`Apakah Anda ingin menghapus tugas ${nama_tugas} ?`);
        if(confirmation) {
          document.getElementById(idForm).submit();
        }
      }
      
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
