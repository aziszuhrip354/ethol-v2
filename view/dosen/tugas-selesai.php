<?php
    session_start();
    include('../../controller/auth.php');
    include('../../controller/middleware.php');
    include('../../controller/helper-func.php');
    include("../../controller/middleware-dosen.php");

    $idMatkul = base64_decode($_GET['class_id']);
    $querySelectMatkul = sqlSelect($connectingToDb,"*","matkul","WHERE id=".$idMatkul);
    $getDataTotalMatkul = mysqli_fetch_assoc($querySelectMatkul);
    $querySelectKelas = sqlSelect($connectingToDb,"*","kelas","WHERE id=".$getDataTotalMatkul['kelas_id']);
    $queryDataTugas = sqlSelect($connectingToDb,"*","tugas","WHERE status='0' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugas = mysqli_num_rows($queryDataTugas);
    $queryDataMateri = sqlSelect($connectingToDb,"*","materi","WHERE matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataMateri = mysqli_num_rows($queryDataMateri);
    $queryDataTugasSelesai = sqlSelect($connectingToDb,"*","tugas","WHERE status='1' AND matkul_id=".$getDataTotalMatkul['id']);                                            
    $getTotalDataTugasSelesai = mysqli_num_rows($queryDataTugasSelesai);
    $getDataTotalKelas = mysqli_fetch_assoc($querySelectKelas);
    $getDataAll = sqlSelect($connectingToDb,"m.nrp , m.nama, m.jenis_kelamin, m.kota_asal","anggota_kelas ak","INNER JOIN mahasiswa m ON m.id=ak.mahasiswa_id
                                            WHERE ak.kelas_id=".$getDataTotalMatkul['kelas_id']."");
    date_default_timezone_set("Asia/Jakarta");
    // $getDataAll =  mysqli_fetch_assoc($getDataAll);
    // dd($getDataTotalMatkul['kelas_id']);
    if(!$getDataTotalMatkul) {
        header('Location: ../dosen/daftar-kelas.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Halaman &rsaquo; Daftar Kontak</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    .jumbotron {
      padding: 2rem 2rem;
      background-color: #ffffff!important;
      box-shadow: 0 0 5px #cacaca!important;
    }

    .btn:not(.btn-social):not(.btn-social-icon):active, .btn:not(.btn-social):not(.btn-social-icon):focus, .btn:not(.btn-social):not(.btn-social-icon):hover {
        border-color: transparent !important;
        background-color: #009c1b!important;
        color: white!important;
    }

    .bdr {
        border: 1px solid #029c1c!important;
        box-shadow: 0 0 1px 1px #029c1c!important;
    }
  </style>

  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link rel="stylesheet" href="../../assets/css/components.css">
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <?php
        include("../navbar.php");
        include("../sidebar.php");
      ?>

      <!-- Main Content -->
      
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?= $getDataTotalMatkul['nama_matkul']?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="../dosen/dashboard.php">Dashboard</a></div>
              <div class="breadcrumb-item active"><a href="../dosen/daftar-kelas.php">Kelas</a></div>
              <div class="breadcrumb-item active"> <a href="../dosen/daftar-kelas.php">Daftar Kelas <?= $_SESSION['users']['dosen']['nama']?></a></div>
              <div class="breadcrumb-item active">Kelas <?= $getDataTotalMatkul['nama_matkul']?></div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Kelas <?= $getDataTotalMatkul['nama_matkul']?> </h2>
            <p class="section-lead">Halaman yang menyajikan data mata kuliah <?= $getDataTotalMatkul['nama_matkul']." kelas ".$getDataTotalKelas['nama_kelas']?></p>
            <div class="">
              <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                  <?php if($_SESSION['error_message']) {?>
                    <div class="m-2 py-2 text-danger rounded border border-danger text-center" role="alert">
                        <?= getErrorMsg() ?>
                        </div>
                    <?php }?>
                    <?php if($_SESSION['success_message']) {?>
                    <div class="m-2 py-2 text-success rounded border border-success text-center" role="alert">
                        <?= getSuccessMsg() ?>
                        </div>
                    <?php }?>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="">Kelas : </h5>
                                <p class="lead"><?=$getDataTotalKelas['nama_kelas']?>.</p>
                                <h5 class="">Mata Kuliah :</h5>
                                <p><?=$getDataTotalMatkul['nama_matkul']?></p>
                                <h5 class="">Jadwal :</h5>
                                <p><?=$getDataTotalMatkul['jam_kelas']?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <a href="../dosen/kelas.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1 ">
                                                    <div class="card-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Mahasiswa</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= mysqli_num_rows($getDataAll) ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../dosen/materi.php?class_id=<?= base64_encode($getDataTotalMatkul['id'])?>">
                                                <div class="card card-statistic-1">
                                                    <div class="card-icon bg-danger">
                                                    <i class="fas fa-book"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Materi</h4>
                                                    </div>
                                                    <div class="card-body">
                                                    <?= $getTotalDataMateri ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="../dosen/tugas-aktif.php?class_id=<?= base64_encode($getDataTotalMatkul['id']) ?>">
                                                <div class="card card-statistic-1 ">
                                                    <div class="card-icon bg-warning">
                                                    <!-- <i class="far fa-pencil"></i> -->
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Aktif</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?= $getTotalDataTugas ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#">
                                                <div class="card card-statistic-1 bdr">
                                                    <div class="card-icon bg-success">
                                                    <i class="fas fa-pen"></i>
                                                    </div>
                                                    <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Tugas Selesai</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?=$getTotalDataTugasSelesai ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <?php  if($getDataTotalMatkul['link_meet']) { ?>
                                <div class="col">
                                    <a href="<?= $getDataTotalMatkul['link_meet'] ?>" target="_blank" class="fw-bold btn border-success w-100 text-success">Masuk Kelas Online</a>
                                </div>
                            <?php } ?>
                                <div class="col">
                                    <a href="javascript:void(0)" onclick="openModal();" class="fw-bold btn border-warning w-100 text-warning"><?= $getDataTotalMatkul['link_meet'] ? "Ganti Link Meet Kelas Online" : "Setting Link Meet Online" ?></a>
                                </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                <div class="card-header">
                        <h4>Penilaian Tugas Mata Kuliah <?= $getDataTotalMatkul['nama_matkul']." ".$getDataTotalKelas['nama_kelas']?></h4>
                      </div>
                      <div class="container-fluid">
                        <div class="row">
                            <table class="table table-striped">
                            <tbody><tr>
                              <th>#</th>
                              <th class="text-center">Nama Tugas</th>
                              <th class="text-center">Total Pengumpul</th>
                              <th class="text-center">Deadline</th>
                            </tr>
                            <?php if($getTotalDataTugasSelesai > 0) { ?>
                              <div class="accordion" id="accordionExample">
                                    <?php $counter = 1; while($rows = mysqli_fetch_assoc($queryDataTugasSelesai)) {?>
                                        <?php
                                            $tugasId = $rows['id'];
                                            $totalAnswer = sqlSelect($connectingToDb, "*", "mahasiswa m","INNER JOIN tugas t ON t.id='$tugasId'
                                                                                                          INNER JOIN submit_tugas st ON st.tugas_id=t.id
                                                                                                          WHERE st.mahasiswa_id=m.id");
                                            $totalDataAnswer = mysqli_num_rows($totalAnswer);                                                                                                          

                                        ?>
                                        <tr>
                                            <td><?= $counter++?></td>
                                            <td>
                                                <?= $rows['nama_tugas'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $totalDataAnswer ?>
                                            </td>
                                            <td class="text-center"><?= date("d M Y H:i",strtotime($rows['deadline'])) ?></td>
                                            
                                            <!-- <td>
                                                <div class="table-links">
                                                <div class="bullet"></div>
                                                <a href="javascript:void(0);"  onclick="openModalFormAdd('<?= $rows['id'] ?>','<?= $rows['nama_kelas'] ?>','<?=$dataDosen['id']?>');" class="text-success">Tambah Mahasiswa</a>
                                                <div class="bullet"></div>
                                                <a href="javascript:void(0);" onclick="deleteClass('<?= $rows['id'] ?>','<?= $rows['nama_kelas'] ?>','DeleteKelas<?= $rows['id'] ?>');" class="text-danger">Hapus Kelas</a>
                                                <div class="bullet"></div>
                                                <a href="daftar-mata-kuliah.php?kode_kelas=<?= base64_encode($rows['id']) ?>" class="text-info">Mata kuliah <?= $rows['nama_kelas'] ?></a>
                                                </div>
                                                <form action="../../controller/DeleteKelas.php" method="POST" id='DeleteKelas<?= $rows['id'] ?>'>
                                                    <input type="hidden" name="idKelas" value="<?= $rows['id'] ?>">
                                                    <input type="hidden" name="namaKelas" value="<?= $rows['nama_kelas'] ?>">
                                                </form>
                                            </td> -->
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div class="">
                                                    <div class="card-header" id="headingOne">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $rows['id'] ?>" aria-expanded="false" aria-controls="collapse<?= $rows['id'] ?>">
                                                                Detail Pengumpulan
                                                            </button>
                                                        </h2>
                                                    </div>

                                                    <div id="collapse<?= $rows['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        
                                                        <div>Data Pengumpul : </div>
                                                        <table class="table table-striped">
                                                            <tbody><tr>
                                                            <th>#</th>
                                                            <th class="text-center">NRP</th>
                                                            <th class="text-center">Nama</th>
                                                            <th class="text-center">Nilai</th>
                                                            </tr>
                                                            <?php if($totalDataAnswer > 0) { ?>
                                                            <div class="accordion" id="accordionExample">
                                                                    <?php $counter2 = 1; while($rows = mysqli_fetch_assoc($totalAnswer)) {?>
                                                                        <tr>
                                                                            <td>
                                                                               <?=$counter2++?>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <?= $rows['nrp'] ?>
                                                                            </td>
                                                                            <td class="text-center"> <?= $rows['nama'] ?></td>
                                                                            <td class="text-center"> <?= $rows['score'] ?? "-" ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <div class="">
                                                                                    <div class="card-header" id="headingOne">
                                                                                        <h2 class="mb-0">
                                                                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $rows['id'] ?>" aria-expanded="false" aria-controls="collapse<?= $rows['id'] ?>">
                                                                                                Detail Jawaban <?= $rows['nama'] ?>
                                                                                            </button>
                                                                                        </h2>
                                                                                    </div>

                                                                                    <div id="collapse<?= $rows['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                                                    <div class="card-body">
                                                                                        
                                                                                    <div class="card-body">
                                                                                            <h6>Pesan Dari <?=$rows['nama']?></h6>
                                                                                            <div class="card card p-2 border border-gray">
                                                                                                <?= $rows['message']?>
                                                                                            </div>
                                                                                            <h6>Waktu Pengumpulan</h6>
                                                                                            <div class="card card p-2 border border-gray"><?=date("d M Y H:i:s", strtotime($rows['submited_at']))?></div>
                                                                                            <div class="form-group">
                                                                                                <label for="" class="">File Tugas</label>
                                                                                                <div class="input-group">
                                                                                                        <label for="inputFile" class="form-control font-italic" id="labelCLick"> <?=$rows['name']?></label>
                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text text-success">
                                                                                                            <a href="../../controller/DownloadTugasControllerr.php?code_file=<?= base64_encode($rows['id']) ?>">
                                                                                                                <i class="fa fa-download"></i>
                                                                                                            </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php if($rows['corrected_at']) { ?>
                                                                                                <div class="">
                                                                                                    <button  onclick="openModal2('<?=$rows['id']?>','<?=$rows['nama']?> (<?=$rows['nrp']?>)','<?=$rows['nama_tugas']?>','<?=$rows['score'] ?? '' ?>');" class="btn w-100 btn-info">
                                                                                                        Edit Nilai
                                                                                                    </button>
                                                                                                    </div>
                                                                                            <?php } else { ?>
                                                                                                        <div class="">
                                                                                                    <button onclick="openModal2('<?=$rows['id']?>','<?=$rows['nama']?> (<?=$rows['nrp']?>)','<?=$rows['nama_tugas']?>','<?=$rows['score'] ?? '' ?>');" class="btn w-100 btn-success">
                                                                                                        Beri Nilai
                                                                                                    </button>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                    <?php }?>
                                                            </div>
                                                            <?php } else {?>
                                                            <tr><td colspan="4" class="text-center fw-bold font-italic">Data kosong</td></tr>
                                                            <?php } ?>
                                                        </tbody></table>
                                                    </div>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        
                                    <?php }?>
                              </div>
                            <?php } else {?>
                              <tr><td colspan="4" class="text-center fw-bold font-italic">Data kosong</td></tr>
                            <?php } ?>
                          </tbody></table>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Edit Link Meet </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="../../controller/UpdateLinkMeet.php" method="POST" id="formEdit">
                <input type="hidden" name="idMatkul" value="<?= $getDataTotalMatkul['id'] ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Link Meet </label>
                    <input type="text" id="link_meet" name="link_meet" value="<?= $getDataTotalMatkul['link_meet'] ?>"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan link meeting kelas online...">
                </div>
            </form>
                <button type="button" id="submitEditBtn" class="btn btn-primary w-100">Simpan</button>
        </div>
        </div>
    </div>
    </div>
  
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLabel">Beri Nilai </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="../../controller/BeriNilai.php" method="POST" id="formBeriNilai">
                  <input type="hidden" name="idTugas" id="idTugas">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Tugas </label>
                      <input type="text" disabled id="beri_nilai_nama_tugas" name="beri_nilai_nama_tugas"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama tugas">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Mahasiswa (NRP)</label>
                      <input type="text" disabled id="beri_nilai_nama_mahasiswa" name="beri_nilai_nama_mahasiswa"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama tugas">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nilai</label>
                      <input type="number" min="0" id="beri_nilai_nilai" name="beri_nilai_nilai"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nilai">
                  </div>
              </form>
                  <button type="button" id="submitBeriNilai" class="btn btn-primary w-100">Simpan</button>
          </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLabel">Tambah Tugas </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="../../controller/BuatTugasController.php" method="POST" id="formTugas">
                  <input type="hidden" name="idMatkul" value="<?= $getDataTotalMatkul['id'] ?>">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama Tugas </label>
                      <input type="text" id="nama_tugas" name="nama_tugas"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan nama tugas">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Deskripsi Tugas </label>
                      <textarea id="default-editor" name="deskripsi"></textarea>
                  </div>
                  <div class="form-group">
                      <label for="">Deadline</label>
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="">Hari</label>
                              <input type="date" name="deadline_hari" class="form-control">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="">Jam</label>
                              <input type="time" name="deadline_waktu" class="form-control">
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </form>
                  <button type="button" id="submitFormTugas" class="btn btn-primary w-100">Simpan</button>
          </div>
          </div>
      </div>
    </div>
  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../../assets/js/stisla.js"></script>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@1/dist/tinymce-jquery.min.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../../assets/js/scripts.js"></script>
  <script src="../../assets/js/custom.js"></script>
  <script>
      tinymce.init({
        selector: 'textarea#default-editor'
      });

      tinymce.init({
        selector: 'textarea#default-editor2'
      });

      openModal = () => {
          $("#exampleModal").modal("show");
      }

      openModal2 = (idTugas, nama, namaTugas, score) => {
          $("#exampleModal2").modal("show");
          $("#beri_nilai_nama_tugas").val(namaTugas);
          $("#beri_nilai_nama_mahasiswa").val(nama);
          $("#beri_nilai_nilai").val(score);
          $("#idTugas").val(idTugas);
          
      }

      openModal3 = (idTugas, nama_tugas, hari, waktu, desc) => {
        $("#exampleModal3").modal("show");
        $("[name='edit_nama_tugas']").val(nama_tugas);
        $("[name='edit_deadline_hari']").val(hari);
        $("[name='edit_deadline_waktu']").val(waktu);
        $("[name='edit_deskripsi']").html(desc);
        $("[name='idTugas']").val(idTugas);
        tinymce.get("default-editor2").setContent(desc);
      }

      $("#submitEditBtn").on("click",() => {
        let form = document.getElementById('formEdit');
        let linkMeet = $("#link_meet").val();
        if(!linkMeet) {
            alert("Maaf... silahkan isi dahulu link meetnya");
        } else {
            form.submit();
        }
      });

      $("#submitBeriNilai").on("click", () => {
        let form = document.getElementById("formBeriNilai");
        let nilai = $("#beri_nilai_nilai").val();
        if(!nilai) {
            alert("Silahkan masukan nilai dahulu!");
        } else {
            form.submit();
        }
      });
      
  </script>
  <!-- Page Specific JS File -->
</body>
</html>
